<?php

use App\Http\Controllers\Api\Auth\AuthController;
use App\Http\Controllers\Api\BackOffice\SaucerController;
use App\Http\Controllers\Api\BackOffice\ProductController;
use App\Http\Controllers\Api\BackOffice\WeeklySaucerController;
use App\Http\Controllers\Api\BackOffice\SaucerCategoryController;
use App\Http\Controllers\Api\BackOffice\ProductCategoryController;
use App\Http\Controllers\Api\BackOffice\RestaurantController;
use App\Http\Controllers\Api\Shared\ShoppingCartController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/*
* Ruta principal de la app
*/

Route::group(['prefix' => 'v1'], function () {

    Route::post('/login', [AuthController::class, 'login']);

    Route::group(['middleware' => 'auth:api'], function () {
        Route::post('/logout', [AuthController::class, 'logout']);
    });

    /*
    * Grupo de compras de un platillo
    */
    Route::group(['middleware' => ['auth:api', 'role:chef']], function () {
    });
    Route::group(['prefix' => 'saucer'], function () {
        Route::group(['middleware' => ['auth:api', 'role:chef']], function () {


        });
        Route::post('/register', [SaucerController::class, 'registerSaucer']);
        Route::get('/list/{restaurant_id}', [SaucerController::class, 'getAllSaucers']);
        Route::get('/show/{restaurant_id}/{saucer_id}', [SaucerController::class, 'showSaucer']);
        Route::get('/show_by_name/{restaurant_id}/{saucer_name}', [SaucerController::class, 'getSaucerByName']);
        Route::put('/update', [SaucerController::class, 'updateSaucer']);
        Route::delete('/delete/{restaurant_id}/{saucer_id}', [SaucerController::class, 'deleteSaucer']);
    });
    /*
    * Grupo de rutas del carro de compras
    */
    Route::group(['prefix' => 'shopping_cart'], function () {
        Route::group(['middleware' => ['auth:api', 'role:customer']], function () {

        });
        Route::post('/add_item', [ShoppingCartController::class, 'addItem']);
        Route::delete('/delete_item/{item_id}', [ShoppingCartController::class, 'deleteItem']);
        Route::get('/show/{shoppingCartId}', [ShoppingCartController::class, 'showShoppingCart']);
        Route::post('/add_item', [ShoppingCartController::class, 'addItem']);
        Route::get('/show/{shoppingCartId}', [ShoppingCartController::class, 'showShoppingCart']);
        Route::post('/billofsale', [ShoppingCartController::class, 'BillOfSale']);
        Route::get('/billofsalelist', [ShoppingCartController::class, 'BillOfSaleList']);
        Route::delete('/deletebillofsale/{id}', [ShoppingCartController::class, 'deleteBillOfSale']);

    });
    /*
    *Grupo de rutas de un restaurante
     */
    Route::group(['prefix' => 'restaurants'], function () {
        Route::group(['middleware' => ['auth:api', 'role:chef']], function () {


        });
        Route::delete('/delete/{restaurant_id}', [RestaurantController::class, 'deleteRestaurant']);
        Route::post('/register', [RestaurantController::class, 'registerRestaurant']);
        Route::get('/list', [RestaurantController::class, 'getAllRestaurants']);
    });
    /*
    * Grupo de rutas de un producto
    * Cuando se realice la parte de logeo se se quita el restaurant de la url
    * Falta listar productos por categoria
    */
    Route::group(['prefix' => 'product'], function () {
        Route::group(['middleware' => ['auth:api', 'role:chef']], function () {

        });
        Route::post('/register', [ProductController::class, 'registerProduct']);
        Route::put('/update', [productController::class, 'updateProduct']);
        Route::delete('/delete/{restaurant_id}/{product_id}', [ProductController::class, 'deleteProduct']);
        Route::get('/list/{restaurant_id}', [ProductController::class, 'getAllProducts']);
        Route::get('/show/{restaurant_id}/{product_id}', [ProductController::class, 'showProduct']);
        Route::get('/show_by_name/{restaurant_id}/{product_name}', [ProductController::class, 'getProductByName']);

    });
    /*
    * Grupo de rutas de una categoria de saucer
    */
    Route::group(['prefix' => 'category_saucer'], function () {
        Route::group(['middleware' => ['auth:api', 'role:chef']], function () {

        });
        Route::post('/register', [SaucerCategoryController::class, 'registerCategory']);
        Route::put('/update', [SaucerCategoryController::class, 'updateCategory']);
        Route::delete('/delete/{saucer_category_id}', [SaucerCategoryController::class, 'deleteCategory']);
        Route::get('/list', [SaucerCategoryController::class, 'getAllCategories']);
        Route::get('/show/{saucer_category_id}', [SaucerCategoryController::class, 'showCategory']);
        Route::get('/show_by_name/{saucer_category_name}', [SaucerCategoryController::class, 'getCategoryByName']);
        //Categorias que tienen platillos en un restaurante
        Route::get('/list_restaurant/{saucer_category_id}', [SaucerCategoryController::class, 'getAllCategoriesByRestaurant']);
    });

    /*
    * Grupo de rutas de una categoria de producto
    */
    Route::group(['prefix' => 'category_product'], function () {
        Route::group(['middleware' => ['auth:api', 'role:chef']], function () {

        });
        Route::post('/register', [ProductCategoryController::class, 'registerCategory']);
        Route::put('/update', [ProductCategoryController::class, 'updateCategory']);
        Route::delete('/delete/{product_category_id}', [ProductCategoryController::class, 'deleteCategory']);
        Route::get('/list', [ProductCategoryController::class, 'getAllCategories']);
        Route::get('/show/{product_category_id}', [ProductCategoryController::class, 'showCategory']);
        Route::get('/show_by_name/{product_category_name}', [ProductCategoryController::class, 'getCategoryByName']);
        //Categorias que tienen productos en un restaurante
        Route::get('/list_restaurant/{product_category_id}', [ProductCategoryController::class, 'getAllCategoriesByRestaurant']);
    });

    /*
    * Grupo de rutas para el semanario de platos
    */
    Route::group(['prefix' => 'weekly_saucer'], function () {
        Route::group(['middleware' => ['auth:api', 'role:chef']], function () {

        });
        Route::post('/register', [WeeklySaucerController::class, 'registerWeeklySaucer']);
        Route::post('/list_date/{restaurant_id}/{date}', [WeeklySaucerController::class, 'getWeeklySaucersDate']);
    });
});
