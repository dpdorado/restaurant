<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        //creacion de roles
        Role::create(['name' => 'customer']);
        Role::create(['name' => 'chef']);
        
        //Creacion de los users

        $chefs = User::factory()
            ->times(2)
            ->create();
        $i = 1;
        foreach ($chefs as $chef) {
            $chef->email = 'chef-' . $i . '@universal-restaurant.com';
            $chef->assignRole('chef');
            $chef->save();
            $i++;
        }
        $customers = User::factory()
            ->times(8)
            ->create();
        $i = 1;
        foreach ($customers as $customer) {
            $customer->email = 'customer-' . $i . '@universal-restaurant.com';
            $customer->assignRole('customer');
            $customer->save();
            $i++;
        }
    }
}
