<?php

namespace Database\Seeders;

use App\Models\CategoryProduct;
use App\Models\SaucerCategory;
use App\Models\Saurce;
use App\Models\ShoppingCart;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // User::factory(10)->create();
        
        //Categorias de platillo
        SaucerCategory::create([
            'name'=>'Comida de mar',
        ]);
        SaucerCategory::create([
            'name'=>'Platos Italianos',
        ]);
        SaucerCategory::create([
            'name'=>'Comida colombiana',
        ]);

        CategoryProduct::create([
            'name'=>'Lacteos'
        ]);
        CategoryProduct::create([
            'name'=>'Frutas'
        ]);
        CategoryProduct::create([
            'name'=>'Principios'
        ]);



        $this->call(UserSeeder::class);
        
        ShoppingCart::create([
            'user_id'=>1,
        ]);
    }
}
