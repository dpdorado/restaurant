<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateproductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('description');
            $table->float('cost');
            $table->integer('stock');
            $table->string('image');
            $table->unsignedBigInteger('categories_product_id');
            $table->unsignedBigInteger('restaurant_id');

            $table->timestamps();

            $table->foreign('categories_product_id')->references('id')->on('categories_products');
            $table->foreign('restaurant_id')->references('id')->on('restaurants')->onDelete('cascade');
        });

        Schema::create('products_saucers', function (Blueprint $table) {
            $table->id();
            $table->enum('default', [true, false])->nullable();
            
            $table->unsignedBigInteger('product_id');
            $table->unsignedBigInteger('saucer_id');

            $table->timestamps();

            $table->foreign('product_id')->references('id')->on('products')->onDelete('cascade');
            $table->foreign('saucer_id')->references('id')->on('saucers')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
        Schema::dropIfExists('products_saucers');
    }
}
