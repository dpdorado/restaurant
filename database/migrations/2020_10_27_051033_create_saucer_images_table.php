<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSaucerImagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('saucer_images', function (Blueprint $table) {
            $table->id();
            $table->string('path')->nullable();

            $table->unsignedBigInteger('saucer_id');

            $table->timestamps();

            $table->foreign('saucer_id')->references('id')->on('saucers')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('saucer_images');
    }
}

