<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWeeklySaucerTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('weekly_saucer', function (Blueprint $table) {
            $table->id();
            $table->Date('fecha');            

            $table->unsignedBigInteger('saucer_id');            
            $table->unsignedBigInteger('restaurant_id');
            
            $table->foreign('saucer_id')->references('id')->on('saucers')->onDelete('cascade');
            $table->foreign('restaurant_id')->references('id')->on('restaurants')->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('weekly_saucer');
    }
}
