<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    /**
    * @OA\Info(title="API Restaurant", description="API Rest developer by restaurant soft(https://endurance-software.com.co) Laravel 8x ", version="1.0")
    *
    * @OA\Server(url="http://localhost/restaurant/public")
    *
    *
    */
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
}
