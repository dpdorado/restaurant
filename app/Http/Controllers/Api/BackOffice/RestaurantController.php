<?php

namespace App\Http\Controllers\Api\BackOffice;

use App\Http\Controllers\Controller;
use App\Interfaces\Application\IRestaurant;
use Illuminate\Http\Request;

class RestaurantController extends Controller
{
    protected $restaurantManager;

    public function __construct(IRestaurant $restaurantManager)
    {
        $this->restaurantManager = $restaurantManager;
    }

    public function registerRestaurant(Request $request)
    {
        $result = $this->restaurantManager->registerRestaurant($request->all());

        $result->addMessage('[RESTERED] # restaurant registered');
        $result->setDescription('restaurant registered');

        $result->setCode(200);

        return $result->getJsonResponse();
    }

    public function getAllRestaurants()
    {

        $result = $this->restaurantManager->getAllRestaurants();

        $result->addMessage('[RESTAURANT] # Restaurants found');
        $result->setDescription('Restaurants retrieved successfully');

        $result->setCode(200);

        return $result->getJsonResponse();
    }
    public function deleteRestaurant($restaurant_id)
    {

        $result = $this->restaurantManager->deleteRestaurant($restaurant_id);

        $result->addMessage('[DELETE] # restaurant deleted');
        $result->setDescription('Restaurant deleted successfully.');
        $result->setCode(200);
        return $result->getJsonResponse();
    }
/*
    public function showSaucer($saucer_id)
    {
        $result = $this->saucerManager->showSaucer($saucer_id);

        $result->addMessage('[SUCCESS] # Saucer found');
        $result->setDescription('Saucer retrieved successfully');

        $result->setCode(200);

        return $result->getJsonResponse();
    }

    public function getSaucerByName($saucer_name)
    {
        $result = $this->saucerManager->getSaucerByName($saucer_name);

        $result->addMessage('[SUCCESS] # Saucers found');
        $result->setDescription('Saucers retrieved successfully');

        $result->setCode(200);

        return $result->getJsonResponse();
    }

    public function updateSaucer(Request $request)
    {
        $result = $this->saucerManager->updateSaucer($request->all());

        $result->addMessage('[UPDATED] # Saucer updated');
        $result->setDescription('Saucer updated successfully');

        $result->setCode(200);

        return $result->getJsonResponse();
    }
*/



}
