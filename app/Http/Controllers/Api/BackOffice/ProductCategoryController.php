<?php

namespace App\Http\Controllers\Api\BackOffice;

use App\Http\Controllers\Controller;
use App\Interfaces\Application\IProductCategory;
use Illuminate\Http\Request;

class ProductCategoryController extends Controller
{
    protected $categoryManager;

    public function __construct(IProductCategory $categoryManager)
    {

        $this->categoryManager = $categoryManager;
    }    

    public function registerCategory(Request $request)
    {        
        $result = $this->categoryManager->registerCategory($request->all());

        $result->addMessage('[RESTERED] # resource registered');
        $result->setDescription('category registered');

        $result->setCode(200);

        return $result->getJsonResponse();
    }

    public function getAllCategories()
    {                
        $result = $this->categoryManager->getAllCategories(); 

        $result->addMessage('[SUCCESS] # Categories found');
        $result->setDescription('Categories retrieved successfully');

        $result->setCode(200);

        return $result->getJsonResponse();
    }    
    public function getAllCategoriesByRestaurant($restaurant_id)
    {        
        $result = $this->categoryManager->getCategoriesByRestaurant($restaurant_id); 

        $result->addMessage('[SUCCESS] # Categories found');
        $result->setDescription('Categories retrieved successfully');

        $result->setCode(200);

        return $result->getJsonResponse();
    }        


    public function showCategory($product_category_id)
    {
        $result = $this->categoryManager->findCategory($product_category_id);

        $result->addMessage('[SUCCESS] # Category found');
        $result->setDescription('Category retrieved successfully');

        $result->setCode(200);

        return $result->getJsonResponse();
    }

    public function getCategoryByName($product_category_name)
    {
        $result = $this->categoryManager->getCategoryByName($product_category_name);

        $result->addMessage('[SUCCESS] # Categories found');
        $result->setDescription('Categories retrieved successfully');

        $result->setCode(200);

        return $result->getJsonResponse();
    }

    public function updateCategory(Request $request)
    {
        $result = $this->categoryManager->updateCategory($request->all());

        $result->addMessage('[UPDATED] # Category updated');
        $result->setDescription('Category updated successfully');

        $result->setCode(200);

        return $result->getJsonResponse();
    }

    public function deleteCategory($product_category_id)
    {

        $result = $this->categoryManager->deleteCategory($product_category_id);

        $result->addMessage('[DELETE] # Category deleted');
        $result->setDescription('Category deleted successfully.');

        $result->setCode(200);

        return $result->getJsonResponse();
    }
}
