<?php

namespace App\Http\Controllers\Api\BackOffice;

use App\Http\Controllers\Controller;
use App\Interfaces\Application\ISaucer;
use Illuminate\Http\Request;

class SaucerController extends Controller
{
    protected $saucerManager;

    public function __construct(ISaucer $saucerManager)
    {

        $this->saucerManager = $saucerManager;
    }

    public function registerSaucer(Request $request)
    {        
        $result = $this->saucerManager->registerSaucer($request->all());

        $result->addMessage('[RESTERED] # resource registered');
        $result->setDescription('saucer registered');

        $result->setCode(200);

        return $result->getJsonResponse();
    }

    public function getAllSaucers($restaurant_id)
    {        
        $result = $this->saucerManager->getAllSaucers($restaurant_id); 

        $result->addMessage('[SUCCESS] # Saucers found');
        $result->setDescription('Saucers retrieved successfully');

        $result->setCode(200);

        return $result->getJsonResponse();
    }

    public function showSaucer($restaurant_id,$saucer_id)
    {
        $result = $this->saucerManager->findSaucer($restaurant_id,$saucer_id);

        $result->addMessage('[SUCCESS] # Saucer found');
        $result->setDescription('Saucer retrieved successfully');

        $result->setCode(200);

        return $result->getJsonResponse();
    }

    public function getSaucerByName($restaurant_id,$saucer_name)
    {
        $result = $this->saucerManager->getSaucerByName($restaurant_id,$saucer_name);

        $result->addMessage('[SUCCESS] # Saucers found');
        $result->setDescription('Saucers retrieved successfully');

        $result->setCode(200);

        return $result->getJsonResponse();
    }

    public function updateSaucer(Request $request)
    {
        $result = $this->saucerManager->updateSaucer($request->all());

        $result->addMessage('[UPDATED] # Saucer updated');
        $result->setDescription('Saucer updated successfully');

        $result->setCode(200);

        return $result->getJsonResponse();
    }

    public function deleteSaucer($restaurant_id,$saucer_id)
    {


        $result = $this->saucerManager->deleteSaucer($restaurant_id,$saucer_id);

        $result->addMessage('[DELETE] # Saucer deleted');
        $result->setDescription('Saucer deleted successfully.');

        $result->setCode(200);

        return $result->getJsonResponse();
    }
}
