<?php

namespace App\Http\Controllers\Api\BackOffice;

use App\Http\Controllers\Controller;
use App\Interfaces\Application\IWeeklySaucer;
use Illuminate\Http\Request;

class WeeklySaucerController extends Controller
{
    protected $weeklyManager;

    public function __construct(IWeeklySaucer $weeklyManager)
    {

        $this->weeklyManager = $weeklyManager;
    }

    public function registerWeeklySaucer(Request $request)
    {        
        $result = $this->weeklyManager->registerWeeklySaucer($request->all());

        $result->addMessage('[RESTERED] # resource registered');
        $result->setDescription('weekly saurce registered');

        $result->setCode(200);

        return $result->getJsonResponse();
    }
   

    public function getWeeklySaucersDate($restaurant_id,$date)
    {
        $result = $this->weeklyManager->findWeeklySaucersDate($restaurant_id,$date);

        $result->addMessage('[SUCCESS] # Saucer found');
        $result->setDescription('Weekly Saucer retrieved successfully');

        $result->setCode(200);

        return $result->getJsonResponse();
    }
}
