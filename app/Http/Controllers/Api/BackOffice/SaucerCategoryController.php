<?php

namespace App\Http\Controllers\Api\BackOffice;

use App\Http\Controllers\Controller;
use App\Interfaces\Application\ISaucerCategory;
use Illuminate\Http\Request;

class SaucerCategoryController extends Controller
{
    protected $categoryManager;

    public function __construct(ISaucerCategory $categoryManager)
    {

        $this->categoryManager = $categoryManager;
    }    

    public function registerCategory(Request $request)
    {        
        $result = $this->categoryManager->registerCategory($request->all());

        $result->addMessage('[RESTERED] # resource registered');
        $result->setDescription('category registered');

        $result->setCode(200);

        return $result->getJsonResponse();
    }

    public function getAllCategories()
    {        
        $result = $this->categoryManager->getAllCategories(); 

        $result->addMessage('[SUCCESS] # Categories found');
        $result->setDescription('Categories retrieved successfully');

        $result->setCode(200);

        return $result->getJsonResponse();
    }    

    public function getAllCategoriesByRestaurant($restaurant_id)
    {        
        $result = $this->categoryManager->getCategoriesByRestaurant($restaurant_id); 

        $result->addMessage('[SUCCESS] # Categories found');
        $result->setDescription('Categories retrieved successfully');

        $result->setCode(200);

        return $result->getJsonResponse();
    }        


    public function showCategory($saucer_category_id)
    {
        $result = $this->categoryManager->findCategory($saucer_category_id);

        $result->addMessage('[SUCCESS] # Category found');
        $result->setDescription('Category retrieved successfully');

        $result->setCode(200);

        return $result->getJsonResponse();
    }

    public function getCategoryByName($saucer_category_name)
    {
        $result = $this->categoryManager->getCategoryByName($saucer_category_name);

        $result->addMessage('[SUCCESS] # Categories found');
        $result->setDescription('Categories retrieved successfully');

        $result->setCode(200);

        return $result->getJsonResponse();
    }

    public function updateCategory(Request $request)
    {
        $result = $this->categoryManager->updateCategory($request->all());

        $result->addMessage('[UPDATED] # Category updated');
        $result->setDescription('Category updated successfully');

        $result->setCode(200);

        return $result->getJsonResponse();
    }

    public function deleteCategory($saucer_category_id)
    {

        $result = $this->categoryManager->deleteCategory($saucer_category_id);

        $result->addMessage('[DELETE] # Category deleted');
        $result->setDescription('Category deleted successfully.');

        $result->setCode(200);

        return $result->getJsonResponse();
    }
}
