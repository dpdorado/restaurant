<?php

namespace App\Http\Controllers\Api\BackOffice;

use App\Http\Controllers\Controller;
use App\Interfaces\Application\ICategory;
use Illuminate\Http\Request;
//Sobra
class CategoryController extends Controller
{
    protected $categoryManager;

    public function __construct(ICategory $categoryManager)
    {

        $this->categoryManager = $categoryManager;
    }    

    public function getAllCategories($restaurant_id)
    {                
        $result = $this->categoryManager->getAllCategories($restaurant_id); 

        $result->addMessage('[SUCCESS] # Categories found');
        $result->setDescription('Categories retrieved successfully');

        $result->setCode(200);

        return $result->getJsonResponse();
    }    
}
