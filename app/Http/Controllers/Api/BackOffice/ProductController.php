<?php

namespace App\Http\Controllers\Api\BackOffice;

use App\Http\Controllers\Controller;
use App\Interfaces\Application\IProduct;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    protected $productManager;

    public function __construct(IProduct $productManager)
    {
        $this->productManager = $productManager;
    }

    public function registerProduct(Request $request)
    {
        $result = $this->productManager->registerProduct($request->all());

        $result->addMessage('[RESTERED] # resource registered');
        $result->setDescription('Product registered');

        $result->setCode(200);

        return $result->getJsonResponse();
    }

    public function getAllProducts($restaurant_id)
    {
        $result = $this->productManager->getAllProducts($restaurant_id);

        $result->addMessage('[SUCCESS] # Products found');
        $result->setDescription('Products retrieved successfully');

        $result->setCode(200);

        return $result->getJsonResponse();
    }

    public function showProduct($restaurant_id, $product_id)
    {
        $result = $this->productManager->findProduct($restaurant_id, $product_id);

        $result->addMessage('[SUCCESS] # Product found');
        $result->setDescription('Product retrieved successfully');

        $result->setCode(200);

        return $result->getJsonResponse();
    }

    public function getProductByName($restaurant_id, $product_name)
    {
        $result = $this->productManager->getProductByName($restaurant_id, $product_name);

        $result->addMessage('[SUCCESS] # Products found');
        $result->setDescription('Products retrieved successfully');

        $result->setCode(200);

        return $result->getJsonResponse();
    }

    public function updateProduct(Request $request)
    {
        $result = $this->productManager->updateProduct($request->all());

        $result->addMessage('[UPDATED] # Product updated');
        $result->setDescription('Product updated successfully');

        $result->setCode(200);

        return $result->getJsonResponse();
    }

    public function deleteProduct($restaurant_id, $product_id)
    {

        $result = $this->productManager->deleteProduct($restaurant_id, $product_id);

        $result->addMessage('[DELETE] # Product deleted');
        $result->setDescription('Product deleted successfully.');

        $result->setCode(200);

        return $result->getJsonResponse();
    }
}
