<?php

namespace App\Http\Controllers\Api\Auth;

use App\Interfaces\Infrastructure\IAuth;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;



class AuthController extends Controller
{
    private $authManager;

    public function __construct(IAuth $authManager)
    {

        $this->authManager = $authManager;
    }

    public function login(Request $request)
    {
        $result = $this->authManager->login($request->all());

        $result->addMessage('[AUTHENTIFIED] # User authentified correctly');
        $result->setDescription('User authentified');
        $result->setStatus('SUCCESS');
        $result->setCode(200);

        return $result->getJsonResponse();
    }

    public function logout()
    {
        $result = $this->authManager->logout();

        $result->addMessage('[LOGOUT] # User session close');
        $result->setDescription('logout');
        $result->setStatus('SUCCESS');
        $result->setCode(200);

        return $result->getJsonResponse();
    }

}
