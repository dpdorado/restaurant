<?php

namespace App\Http\Controllers\Api\Shared;

use App\Interfaces\Application\IShoppingCart;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ShoppingCartController extends Controller
{
    protected $cartManager;

    public function __construct(IShoppingCart $cartManager)
    {
        $this->cartManager = $cartManager;
    }

    public function addItem(Request $request)
    {

        $result = $this->cartManager->addItem($request->all());

        $result->addMessage('ADDED # resource added');
        $result->setDescription('resource added');
        $result->setCode(200);

        return $result->getJsonResponse();
    }
    public function deleteItem($item_id)
    {

        $result = $this->cartManager->deleteItem($item_id);

        $result->addMessage('DELETED # resource removed');
        $result->setDescription('resource removed');
        $result->setCode(200);

        return $result->getJsonResponse();
    }

    public function showShoppingCart($shopping_cart_id)
    {

        $result = $this->cartManager->showShoppingCart($shopping_cart_id); //El metodo no fue definido en la interfaz

        $result->setDescription('resource was found');
        $result->addMessage('FOUND # resource found'); //El mesaje no correcsponde
        $result->setCode(200);

        return $result->getJsonResponse();
    }
    public function BillOfSale(Request $request)
    {

        $result = $this->cartManager->BillOfSale($request->all()); //El metodo no fue definido en la interfaz

        $result->setDescription('resource was found');
        $result->addMessage('FOUND # resource found'); //El mesaje no correcsponde
        $result->setCode(200);

        return $result->getJsonResponse();
    }
    public function BillOfSaleList()
    {
        $result = $this->cartManager->BillOfSaleList(); //El metodo no fue definido en la interfaz

        //$result->setDescription('resource was found');
        //$result->addMessage('FOUND # resource found'); //El mesaje no correcsponde
        //$result->setCode(200);
        return $result;
    }
    public function deleteBillOfSale($id)
    {

        $result = $this->cartManager->deleteBillOfSale($id);

        $result->addMessage('DELETED # resource removed');
        $result->setDescription('resource removed');
        $result->setCode(200);

        return $result->getJsonResponse();
    }

}
