<?php

namespace App\Http\Resources\Api\Shared\Search;

use App\Http\Resources\Api\BackOffice\Index\ProductResource;
use App\Http\Resources\Api\BackOffice\Search\SaucerResource;
use Illuminate\Http\Resources\Json\JsonResource;

class SaleItemResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'quantity' => $this->quantity,
            'unit_price' => $this->unit_price,
            'sub_total' => $this->quantity * $this->unit_price,
            'product' => new ProductResource($this->product),

        ];
    }
}
