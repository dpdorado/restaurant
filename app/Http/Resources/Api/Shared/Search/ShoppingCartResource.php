<?php

namespace App\Http\Resources\Api\Shared\Search;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\DB;

class ShoppingCartResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $total = $this->items()
            ->select(DB::raw('sum(unit_price*quantity) as total'))
            ->first()->total;

        return [
            'id' => $this->id,
            'total' => ($total ? $total : 0),
            'items' => SaleItemResource::collection($this->items)
        ];
    }
}
