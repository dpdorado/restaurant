<?php

namespace App\Http\Resources\Api\BackOffice\Search;

use App\Http\Resources\Api\BackOffice\Index\SaucerCategoryResource;
use App\Http\Resources\Api\BackOffice\Index\SaucerImageResource;
use App\Models\SaucerCategory;
use Illuminate\Http\Resources\Json\JsonResource;

class SaucerResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'description' => $this->description,
            'cost' => $this->cost,
            'category' => new SaucerCategoryResource($this->category),            
            'restaurant_id' => $this->restaurant_id,
            'image' =>$this->image,
        ];
    }
}
