<?php

namespace App\Http\Resources\Api\BackOffice\Search;

use App\Http\Resources\Api\BackOffice\Index\SaucerResource;
use App\Http\Resources\Api\BackOffice\Index\SaucerImageResource;
use App\Models\SaucerCategory;
use Illuminate\Http\Resources\Json\JsonResource;

class SaleItemResource extends JsonResource
{

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return Array([
            'id' => $this->id,
            'shopping_cart_id' => $this->shopping_cart_id,
            'order_id' => $this->order_id,
            'saucer_id' =>new SaucerResource($this->saucer),
            'quantity' => $this->quantity,
            'unit_price' =>$this->unit_price,


        ]);
    }

}
