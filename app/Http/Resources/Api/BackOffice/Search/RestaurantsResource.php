<?php

namespace App\Http\Resources\Api\BackOffice\Search;

use App\Http\Resources\Api\BackOffice\Index\SaucerCategoryResource;
use App\Http\Resources\Api\BackOffice\Index\SaucerImageResource;
use App\Models\SaucerCategory;
use Illuminate\Http\Resources\Json\JsonResource;

class RestaurantsResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'description' => $this->description,
            'direction' => $this->cost,
            'image' =>$this->image,
        ];
    }
}
