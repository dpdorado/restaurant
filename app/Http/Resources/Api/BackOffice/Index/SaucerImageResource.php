<?php

namespace App\Http\Resources\Api\BackOffice\Index;

use Illuminate\Http\Resources\Json\JsonResource;

class SaucerImageResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'=>$this->id,
            'path'=>$this->path
        ];
    }
}
