<?php

namespace App\Http\Resources\Api\BackOffice\Index;

use App\Http\Resources\Api\Shared\Index\ImageResource;
use Illuminate\Http\Resources\Json\JsonResource;

class ProductResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'description'=>$this->description,
            'cost'=>$this->cost,
            'stock'=>$this->stock,
            'image' => $this->image,
            'category_product'=> new CategoryResource($this->category)
        ];
    }
}
