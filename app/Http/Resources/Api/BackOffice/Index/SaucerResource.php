<?php

namespace App\Http\Resources\Api\BackOffice\Index;

use Illuminate\Http\Resources\Json\JsonResource;

class SaucerResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'description' => $this->description,
            'image'=>$this->image,
            'cost' => $this->products()->where('products_saucers.default', true)->sum('cost'),
            'products_defaults' => ProductResource::collection($this->products()->where('products_saucers.default', true)->get()),
            'other_products' => ProductResource::collection($this->products()->where('products_saucers.default', false)->get()),
            'category_saucer' => new CategoryResource($this->category)
        ];
    }
}
