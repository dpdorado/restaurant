<?php

namespace App\Http\Resources\Api\BackOffice\Index;

use Illuminate\Http\Resources\Json\JsonResource;

class WeeklySaucerResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'fecha' => $this->fecha,
            'saucer_id' => $this->saucer_id,
            'restaurant_id' => $this->restaurant_id            
        ];
    }
}
