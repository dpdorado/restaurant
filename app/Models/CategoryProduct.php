<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

//La CategoryProduct tiene una relacion uno a muchos con Product donde un producto
//puede pertenecear a una sola categoria y una categoria puede tener varios productos
class CategoryProduct extends Model
{
    use HasFactory;
    protected $table = 'categories_products';
    protected $fillable = ['name'];

    public function products()
    {
        return $this->hasMany(Product::class);//relacion uno a muchos
    }
}
