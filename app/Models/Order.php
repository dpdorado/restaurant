<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
/*
cuando se genera una compra se crear una orden la cual tiene un costo total de la compra
un estado de confirmado o no y una identificacion

*/
class Order extends Model
{
    use HasFactory;

    protected $table = 'orders';
    protected $fillable = ['total_cost', 'state', 'user_id'];

    public function user()
    {
        return $this->belongsTo(User::class);//relacion de pertenencia donde una orden pertenece a un usuario
    }
    public function items()
    {
        return $this->hasMany(SaleItem::class);//relacion uno a muchos donde un item de venta pertenece a una orden y una orden tiene varios item de venta
    }
}
