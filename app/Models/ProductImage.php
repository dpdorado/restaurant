<?php

namespace App\Models;
/*
ProductoImage modelo que contiene la la direccion de la imagen  del producto "PATH" y el
id del producto respectivo

*/
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProductImage extends Model
{
    use HasFactory;

    protected $table = 'product_images';
    protected $fillable = ['path', 'product_id'];

    public function product()
    {
        return $this->belongsTo(Product::class); //relacion de pertenecina entre  producto y productoImage
    }
}
