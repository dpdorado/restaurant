<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
/*
Carro de compras donde el usuario va ir guardando los platos o productos que quiere comprar
*/
class ShoppingCart extends Model
{
    use HasFactory;

    protected $table = 'shopping_carts';
    protected $fillable = ['user_id'];

    public function items()
    {
        return $this->hasMany(SaleItem::class);//Relacion uno a muchos, donde carro de comprar puede tener varios item de ventas
    }

    public function user()
    {
        return $this->belongsTo(User::class);//Relacion de pertenencia, donde un carro de compara pertenece a un usuario
    }
}
