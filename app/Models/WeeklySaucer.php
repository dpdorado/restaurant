<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
/*
WeeklySaucer modelo que contiene la informacion de la semana de los platos que se ofreceran
de un restaurante respectivo
*/
class WeeklySaucer extends Model{

    use HasFactory;

    protected $table = 'weekly_saucer';
    protected $fillable = ['fecha', 'saucer_id', 'restaurant_id'];

    public function restaurant()
    {
        return $this->belongsTo(Restaurant::class, 'restaurant_id');//Relacion de pertenencia, donde los platos de la semana pertenecen a un restaurante
    }

    public function saucers()
    {
        return $this->belongsTo(Saucer::class, 'saucer_id');//Relacion de pertenencia, donde un plato pertenece a los platos de la semana(WeeklySaucer)
    }
}
