<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
/*
Contiene la informacion para las imagnes de restaurante
*/
class RestaurantImage extends Model
{
    use HasFactory;

    protected $table = 'restaurant_images';
    protected $fillable = ['path', 'restaurant_id'];

    public function restaurant()
    {
        return $this->belongsTo(Restaurant::class);//relacion de pertenencia donde un restaurante le pertenence una imagen
    }
}
