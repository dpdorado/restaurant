<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
/*
SaucerCategoria modelo el cual tiene el nombre de la catergoria que puede
pertenecer un plato(saucer)

*/
class SaucerCategory extends Model
{
    use HasFactory;
    protected $table = 'saucer_category';
    protected $fillable = ['name'];

    public function saucer()
    {
        return $this->hasMany(Saucer::class);//Relacion uno a muchos, donde a un Saucercategory puede pertenecer varios Saucer
    }
}
