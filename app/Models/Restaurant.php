<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
/*
El modelo Restaurant contiene como atributos nombre, descripcion, direccion , dominio, telefono
direcion de almacenamiento de la imagen corespondiente al restaurante.
*/
class Restaurant extends Model
{
    use HasFactory;
    protected $table = 'restaurants';
    protected $fillable = ['name', 'description', 'address', 'domain','phone', 'path','image'];


    /* public function images()
    {
        return $this->hasMany(RestaurantImage::class);//relacion uno a muchos, donde un restaurane le pueden pertenecer varias imagenes
    } */

    public function products()
    {
        return $this->hasMany(Product::class);//Un restaurante puede tener uno o varios productos
    }

    public function saucers()
    {
        return $this->hasMany(Saucer::class);//Un restaurane puede tener uno o varios platos
    }
}
