<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
/*
El modelo producto tiene como atributos un nombre, descripcion, strock, costo, relacion con categoria y restaurante
*/
class Product extends Model
{
    use HasFactory;

    protected $table = 'products';
    protected $fillable = ['name', 'description', 'stock', 'cost','image', 'categories_product_id', 'restaurant_id'];

    public function restaurant()
    {
        return $this->belongsTo(Restaurant::class, 'restaurant_id');//Un producto pertenece a un restaurante
    }

    public function category()
    {
        return $this->belongsTo(CategoryProduct::class, 'categories_product_id');//Un producto pertenece a una categoria
    }

    /* public function images()
    {
        return $this->hasMany(ProductImage::class);// Un producto puede tener una o muchas imagenes
    } */

    public function saucers()
    {
        return $this->belongsToMany(Saucer::class, 'products_saucers');//Relaciòn muchos a muchos entre plato y producto
    }
}
