<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
/*
Este modelo es los item de ventas que pertenece a una orden y estan comformados por productos
*/
class SaleItem extends Model
{
    use HasFactory;

    protected $table = 'sale_items';
    protected $fillable = ['unit_price', 'quantity', 'shopping_cart_id', 'product_id', 'order_id'];

    public function product()
    {
        return $this->belongsTo(Product::class); //Relacion de pertenencia donde, un SaleItem pertenece a un producto
    }

    public function cart()
    {
        return $this->belongsTo(ShoppingCart::class);//Relacion de pertenencia donde un SaleItem pertences a un carrito de comparas
    }

    public function order()
    {
        return $this->belongsTo(Order::class);//Relacion de pertenecia donde, Una orden SaleItem pertenece a una orden
    }
}
