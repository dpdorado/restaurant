<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
/*
El modelo Saucer tiene como atributos nombre,descripcion, la categoria a la que pertenece
y le restaurane al que pertenece

*/
class Saucer extends Model
{
    use HasFactory;

    protected $table = 'saucers';
    protected $fillable = ['name', 'description','image', 'saucer_category_id', 'restaurant_id'];

    public function restaurant()
    {
        return $this->belongsTo(Restaurant::class, 'restaurant_id');//Relacion de pertenencia, donde un saucer pertenecea un restaurant
    }

    public function category()
    {
        return $this->belongsTo(SaucerCategory::class, 'saucer_category_id');//Relacion de pertenencia, donde un Saucer pertenece a una categoria
    }

    /* public function images()
    {
        return $this->hasMany(SaucerImage::class);//Relacion uno a muchos, donde un Saucer le puden pertenecer uno o varias imagenes
    } */

    public function products()
    {
        return $this->belongsToMany(Product::class, 'products_saucers');
    }
}
