<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
/*
Este modelo contiene la direcion de almacenamiento de la imagne para un plato, ademas
de su identificador del plato respectivo

*/
class SaucerImage extends Model
{
    use HasFactory;

    protected $table = 'saucer_images';
    protected $fillable = ['path', 'saucer_id'];

    public function saucer()
    {
        return $this->belongsTo(Saucer::class);//Relacion de pertenencia, donde un SaucerImage pertenece a un plato
    }
}
