<?php

namespace App\Providers;

use App\Interfaces\Application\IRestaurant;
use App\Interfaces\Application\ISaucer;
use App\Interfaces\Application\IProduct;
use App\Interfaces\Application\IPlate;
use App\Interfaces\Application\IShoppingCart;
use App\Interfaces\Application\ISaucerCategory;
use App\Interfaces\Application\IProductCategory;
use App\Interfaces\Application\ICategory;
use App\Interfaces\Application\IWeeklySaucer;
use App\Interfaces\Domain\ISaucer as DomainISaucer;
use App\Interfaces\Domain\IProduct as DomainIProduct;
use App\Interfaces\Domain\IPlate as DomainIPlate;
use App\Interfaces\Domain\IShoppingCart as DomainIShoppingCart;
use App\Interfaces\Domain\IRestaurant as DomainIRestaurant;
use App\Interfaces\Infrastructure\IAuth;
use App\Src\Application\SaucerImp;
use App\Src\Application\ProductImp;
use App\Src\Application\PlateImp;
use App\Src\Application\CategoryImp;
use App\Src\Application\SaucerCategoryImp;
use App\Src\Application\ProductCategoryImp;
use App\Src\Application\RestaurantImpl;
use App\Src\Application\ShoppingCartImpl;
use App\Src\Application\WeeklySaucerImp;
use App\Src\Domain\SaucerImp as DomainSaucerImp;
use App\Src\Domain\PlateImp as DomainPlateImp;
use App\Src\Domain\ProductImp as DomainProductImp;
use App\Src\Domain\ShoppingCartImpl as DomainShoppingCartImpl;
use App\Src\Domain\RestaurantImpl as DomainRestaurantImpl;
use App\Src\Infrastructure\AuthImpl;
use Illuminate\Support\ServiceProvider;

class RestaurantProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //Aplication
        $this->app->bind(ISaucer::class, SaucerImp::class);
        $this->app->bind(IProduct::class, ProductImp::class);    
        $this->app->bind(IShoppingCart::class, ShoppingCartImpl::class);
        $this->app->bind(IRestaurant::class,RestaurantImpl::class);
        $this->app->bind(IWeeklySaucer::class,WeeklySaucerImp::class);
        //$this->app->bind(ICategory::class,CategoryImp::class);
        $this->app->bind(ISaucerCategory::class,SaucerCategoryImp::class);
        $this->app->bind(IProductCategory::class,ProductCategoryImp::class);
        //Domain
        $this->app->bind(DomainIShoppingCart::class, DomainShoppingCartImpl::class);
        $this->app->bind(DomainISaucer::class,DomainSaucerImp::class);
        $this->app->bind(DomainIPlate::class,DomainPlateImp::class);
        $this->app->bind(DomainIProduct::class,DomainProductImp::class);
        $this->app->bind(DomainIRestaurant::class,DomainRestaurantImpl::class);
        //Infrastructure
        $this->app->bind(IAuth::class,AuthImpl::class);


    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
