<?php


/**
 * @OA\Post(
 *     path="/api/v1/restaurants/Register",
 *     tags={"Restaurant"},
 *     summary="Register a new Restaurants",
 *     description="Allows you to register a Restaurant",
 *     operationId="regRestaurant",
 *
 *
 *   @OA\Parameter(
 *         name="Accept",
 *         in="header",
 *         description="application/json",
 *         required=true,
 *         explode=true,
 *     ),
 *
 *      @OA\Parameter(
 *         name="Content-Type",
 *         in="header",
 *         description="application/x-www-form-urlencoded",
 *         required=true,
 *         explode=true,
 *     ),
 *     @OA\Parameter(
 *         name="Authorization",
 *         in="header",
 *         description="Bearer [token]",
 *         required=true,
 *         explode=true,
 *     ),
 *     security={
 *       {"passport": {}},
 *     },
 *     @OA\Parameter(
 *         name="name",
 *         in="query",
 *         description="Restaurant name",
 *         required=true,
 *         explode=true,
 *    ),
 *     @OA\Parameter(
 *         name="description",
 *         in="query",
 *         description="Restaurant description",
 *         required=true,
 *         explode=true,
 *    ),
 *    @OA\Parameter(
 *         name="address",
 *         in="query",
 *         description="restaurant address",
 *         required=true,
 *         explode=true,
 *    ),
 *   @OA\Parameter(
 *         name="phone",
 *         in="query",
 *         description="Restaurant phone",
 *         required=true,
 *         explode=true,
 *    ),
 *   @OA\Parameter(
 *         name="image",
 *         in="query",
 *         description="url image",
 *         required=true,
 *         explode=true,
 *    ),
 * @OA\Parameter(
 *         name="domain",
 *         in="query",
 *         description="restaurant domain",
 *         required=true,
 *         explode=true,
 *    ),
 *     @OA\Response(
 *         response=200,
 *        description="Restaurant registered",
 *      content={
 *             @OA\MediaType(
 *                  mediaType="Success",
 *                  example="true",
 *               ),
 *       @OA\MediaType(
 *                      mediaType="Data",
 *                      example="TODO"
 *
 *                  ),
 *               @OA\MediaType(
 *                      mediaType="Errors",
 *                      example={}
 *
 *               ),
 *         @OA\MediaType(
 *                 mediaType="Messages",
 *                 @OA\Schema(
 *                      @OA\Property(
 *                         property="code_message",
 *                         type="message identifier code",
 *                  ),
 *                  @OA\Property(
 *                         property="message",
 *                         type="description of message",
 *                  ),
 *                  example={
 *                              {
 *                                  "code_message": "[REGISTERED]",
 *                                  "message": "resource registered"
 *                              },
 *                     }
 *                 ),
 *             )
 *
 *        }
 *     )
 * )
 */
///////////listar Restaurant
/**
 * @OA\Get(
 *     path="/api/v1/restaurants/list",
 *     tags={"Restaurant"},
 *     summary="List a new Restaurants",
 *     description="Allows you to List a Restaurant",
 *     operationId="ListRestaurant",
 *
 *     @OA\Response(
 *         response=200,
 *        description="Restaurant list",
 *      content={
 *             @OA\MediaType(
 *                  mediaType="Success",
 *                  example="true",
 *               ),
 *       @OA\MediaType(
 *                      mediaType="Data",
 *                      example="TODO"
 *
 *                  ),
 *               @OA\MediaType(
 *                      mediaType="Errors",
 *                      example={}
 *
 *               ),
 *         @OA\MediaType(
 *                 mediaType="Messages",
 *                 @OA\Schema(
 *                      @OA\Property(
 *                         property="code_message",
 *                         type="message identifier code",
 *                  ),
 *                  @OA\Property(
 *                         property="message",
 *                         type="description of message",
 *                  ),
 *                  example={
 *                              {
 *                                  "code_message": "[REGISTERED]",
 *                                  "message": "resource list"
 *                              },
 *                     }
 *                 ),
 *             )
 *
 *        }
 *     )
 * )
 */
///////////delete restaurant
/**
 * @OA\Delete(
 *     path="/api/v1/restaurants/delete/{restaurant_id}",
 *     tags={"Restaurant"},
 *     summary="Delete Restaurants",
 *     description="Allows you to List a Restaurant",
 *     operationId="DeleteRestaurant",
 *     @OA\Parameter(
 *         name="Authorization",
 *         in="header",
 *         description="Bearer [token]",
 *         required=true,
 *         explode=true,
 *     ),
 *     security={
 *       {"passport": {}},
 *     },
 *     @OA\Response(
 *         response=200,
 *        description="Restaurant Delete",
 *      content={
 *             @OA\MediaType(
 *                  mediaType="Success",
 *                  example="true",
 *               ),
 *       @OA\MediaType(
 *                      mediaType="Data",
 *                      example="TODO"
 *
 *                  ),
 *               @OA\MediaType(
 *                      mediaType="Errors",
 *                      example={}
 *
 *               ),
 *         @OA\MediaType(
 *                 mediaType="Messages",
 *                 @OA\Schema(
 *                      @OA\Property(
 *                         property="code_message",
 *                         type="message identifier code",
 *                  ),
 *                  @OA\Property(
 *                         property="message",
 *                         type="description of message",
 *                  ),
 *                  example={
 *
 *                              {
 *                                  "code_message": "ERR_EXISTS",
 *                                  "field": "restaurant_id",
 *                                  "message": "The selected restaurant id is invalid."
 *                              },
 *
 *                     }
 *                 ),
 *             )
 *
 *        }
 *     )
 * )
 */
