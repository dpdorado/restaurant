<?php
/**
 *  @OA\Post(
 *     path="/api/v1/category_saucer/register",
 *     tags={"SaucerCategory"},
 *     summary="Register a new saucercategory",
 *     description="Allows you to register a saucer category",
 *     operationId="regSaucerCategory",
 *
 *     @OA\Parameter(
 *         name="Accept",
 *         in="header",
 *         description="application/json",
 *         required=true,
 *         explode=true,
 *     ),
 *     @OA\Parameter(
 *         name="Content-Type",
 *         in="header",
 *         description="application/x-www-form-urlencoded",
 *         required=true,
 *         explode=true,
 *     ),
 *     @OA\Parameter(
 *         name="Authorization",
 *         in="header",
 *         description="Bearer [token]",
 *         required=true,
 *         explode=true,
 *     ),
 *     security={
 *       {"passport": {}},
 *     },
 *     @OA\Parameter(
 *         name="name",
 *         in="query",
 *         description="saucer category name",
 *         required=true,
 *         explode=true,
 *    ), 
 *    @OA\Response(
 *         response=200,
 *         description="saucer registered",
 *         content={
 *             @OA\MediaType(
 *                  mediaType="Success",
 *                  example="true",
 *               ),
 *                 @OA\MediaType(
 *                      mediaType="Data",
 *                      example="TODO"
 *                      
 *                  ),
 *               @OA\MediaType(
 *                      mediaType="Errors",
 *                      example={}
 *                      
 *               ),
 *               @OA\MediaType(
 *                 mediaType="Messages",
 *                 @OA\Schema(
 *                      @OA\Property(
 *                         property="code_message",
 *                         type="message identifier code",
 *                  ),
 *                  @OA\Property(
 *                         property="message",
 *                         type="description of message",
 *                  ),
 *                  example={
 *                              {
 *                                  "code_message": "[REGISTERED]",
 *                                  "message": "resource registered"
 *                              },
 *                     }
 *                 ),
 *             )
 *          }
 *     ),
 *     @OA\Response(
 *         response=400,
 *         description="Exist conflict whit the request, please check the errors and messages",
 *         content={
 *                  @OA\MediaType(
 *                      mediaType="Success",
 *                      example="false",
 *                  ),
 *                 @OA\MediaType(
 *                      mediaType="Data",
 *                      example={
 *                      }
 *                  ),
 *                 
 *             @OA\MediaType(
 *                 mediaType="Errors",
 *                 @OA\Schema(
 *                      title="errors",
 *                      @OA\Property(
 *                         property="code_message",
 *                         type="message identifier code",
 *                  ),
 *                  @OA\Property(
 *                         property="field",
 *                         type="field presenting anomaly",
 *                  ),
 *                  @OA\Property(
 *                         property="message",
 *                         type="description of what's going on with the field",
 *                  ),
 *                  example={
 *                              {
 *                                  "code_message": "ERR_REQUIRED",
 *                                  "field": "name",
 *                                  "message": "The name field is required."
 *                              },
 *                              {
 *                                  "code_message": "ERR_EXISTS",
 *                                  "field": "saurce_category_id",
 *                                  "message": "The selected saurce category id is invalid."
 *                              },
 *                              {
 *                                  "code_message": "ERR_ARRAY",
 *                                  "field": "images",
 *                                  "message": "The images must be an array."
 *                              },
 *                     }
 *                 ),
 *             ),
 *               @OA\MediaType(
 *                 mediaType="Messages",
 *                 @OA\Schema(
 *                      title="errors",
 *                      @OA\Property(
 *                         property="code_message",
 *                         type="message identifier code",
 *                  ),
 *                  @OA\Property(
 *                         property="message",
 *                         type="description of message",
 *                  ),
 *                  example={
 *                              {
 *                                  "code_message": "[ERR_CHECK_DATA]",
 *                                  "message": "The form has errors whit the inputs"
 *                              },
 *                     }
 *                 ),
 *             )
 *         }
 *     )
 * )
 * 
 *  @OA\Get(
 *     path="/api/v1/category_saucer/list",
 *     tags={"SaucerCategory"},
 *     summary="List all the categories saucers.",
 *     description="Allows you to list saucers categories",
 *     operationId="listCategoriesSaucers",
 *
 *     @OA\Parameter(
 *         name="Accept",
 *         in="header",
 *         description="application/json",
 *         required=true,
 *         explode=true,
 *     ),
 *     @OA\Parameter(
 *         name="Content-Type",
 *         in="header",
 *         description="application/x-www-form-urlencoded",
 *         required=true,
 *         explode=true,
 *     ),  
 *    @OA\Response(
 *         response=200,
 *         description="resource was found",
 *         content={
 *             @OA\MediaType(
 *                  mediaType="Success",
 *                  example="true",
 *               ),
 *                 @OA\MediaType(
 *                      mediaType="Data",
 *                      example="TODO"
 *                      
 *                  ),
 *               @OA\MediaType(
 *                      mediaType="Errors",
 *                      example={}
 *                      
 *               ),
 *               @OA\MediaType(
 *                 mediaType="Messages",
 *                 @OA\Schema(
 *                      @OA\Property(
 *                         property="code_message",
 *                         type="message identifier code",
 *                  ),
 *                  @OA\Property(
 *                         property="message",
 *                         type="description of message",
 *                  ),
 *                  example={
 *                              {
 *                                  "code_message": "FOUND",
 *                                  "message": "resource found"
 *                              },
 *                     }
 *                 ),
 *             )
 *          }
 *     ),
 * )
 * 
 * @OA\Get(
 *     path="/api/v1/category_saucer/show/{saucer_category_id}",
 *     tags={"SaucerCategory"},
 *     summary="Get saucer category with id.",
 *     description="Allows you to get saucer category",
 *     operationId="getSaucerCategoryId",
 *
 *     @OA\Parameter(
 *         name="Accept",
 *         in="header",
 *         description="application/json",
 *         required=true,
 *         explode=true,
 *     ),
 *     @OA\Parameter(
 *         name="Content-Type",
 *         in="header",
 *         description="application/x-www-form-urlencoded",
 *         required=true,
 *         explode=true,
 *     ), 
 *     @OA\Parameter(
 *         name="saucer_category_id",
 *         in="path",
 *         description="id of the saucer category",
 *         required=true,
 *         explode=true,
 *    ),
 *    @OA\Response(
 *         response=200,
 *         description="resource was found",
 *         content={
 *             @OA\MediaType(
 *                  mediaType="Success",
 *                  example="true",
 *               ),
 *                 @OA\MediaType(
 *                      mediaType="Data",
 *                      example="TODO"
 *                      
 *                  ),
 *               @OA\MediaType(
 *                      mediaType="Errors",
 *                      example={}
 *                      
 *               ),
 *               @OA\MediaType(
 *                 mediaType="Messages",
 *                 @OA\Schema(
 *                      @OA\Property(
 *                         property="code_message",
 *                         type="message identifier code",
 *                  ),
 *                  @OA\Property(
 *                         property="message",
 *                         type="description of message",
 *                  ),
 *                  example={
 *                              {
 *                                  "code_message": "FOUND",
 *                                  "message": "resource was found"
 *                              },
 *                     }
 *                 ),
 *             )
 *          }
 *     ),
 * )
 * 
 * @OA\Get(
 *     path="/api/v1/category_saucer/show_by_name/{saurcer_category_name}",
 *     tags={"SaucerCategory"},
 *     summary="Get saucer category with name.",
 *     description="Allows you to get saucer category.",
 *     operationId="getSaucerCategoryName",
 *
 *     @OA\Parameter(
 *         name="Accept",
 *         in="header",
 *         description="application/json",
 *         required=true,
 *         explode=true,
 *     ),
 *     @OA\Parameter(
 *         name="Content-Type",
 *         in="header",
 *         description="application/x-www-form-urlencoded",
 *         required=true,
 *         explode=true,
 *     ), 
 *     @OA\Parameter(
 *         name="saucer_category_name",
 *         in="path",
 *         description="saucer name",
 *         required=true,
 *         explode=true,
 *    ),
 *    @OA\Response(
 *         response=200,
 *         description="Saucers retrieved successfully",
 *         content={
 *             @OA\MediaType(
 *                  mediaType="Success",
 *                  example="true",
 *               ),
 *                 @OA\MediaType(
 *                      mediaType="Data",
 *                      example="TODO"
 *                      
 *                  ),
 *               @OA\MediaType(
 *                      mediaType="Errors",
 *                      example={}
 *                      
 *               ),
 *               @OA\MediaType(
 *                 mediaType="Messages",
 *                 @OA\Schema(
 *                      @OA\Property(
 *                         property="code_message",
 *                         type="message identifier code",
 *                  ),
 *                  @OA\Property(
 *                         property="message",
 *                         type="description of message",
 *                  ),
 *                  example={
 *                              {
 *                                  "code_message": "[SUCCESS]",
 *                                  "message": "Saucers found"
 *                              },
 *                     }
 *                 ),
 *             )
 *          }
 *     ),
 * )
 * 
 * @OA\Put(
 *     path="/api/v1/category_saucer/update",
 *     tags={"SaucerCategory"},
 *     summary="Update saucer categry with id.",
 *     description="Allows you to update saucer category",
 *     operationId="updateSaucerCategory",
 *
*     @OA\Parameter(
 *         name="Accept",
 *         in="header",
 *         description="application/json",
 *         required=true,
 *         explode=true,
 *     ),
 *     @OA\Parameter(
 *         name="Content-Type",
 *         in="header",
 *         description="application/x-www-form-urlencoded",
 *         required=true,
 *         explode=true,
 *     ),
 *     @OA\Parameter(
 *         name="Authorization",
 *         in="header",
 *         description="Bearer [token]",
 *         required=true,
 *         explode=true,
 *     ),
 *     security={
 *       {"passport": {}},
 *     },
 *     @OA\Parameter(
 *         name="saucer_category_id",
 *         in="query",
 *         description="id of the saucer category",
 *         required=true,
 *         explode=true,
 *     ), 
 *     @OA\Parameter(
 *         name="name",
 *         in="query",
 *         description="saucer category name",
 *         required=false,
 *         explode=true,
 *    ),
 *    @OA\Response(
 *         response=200,
 *         description="saucer updated successfully",
 *         content={
 *             @OA\MediaType(
 *                  mediaType="Success",
 *                  example="true",
 *               ),
 *                 @OA\MediaType(
 *                      mediaType="Data",
 *                      example="TODO"
 *                      
 *                  ),
 *               @OA\MediaType(
 *                      mediaType="Errors",
 *                      example={}
 *                      
 *               ),
 *               @OA\MediaType(
 *                 mediaType="Messages",
 *                 @OA\Schema(
 *                      @OA\Property(
 *                         property="code_message",
 *                         type="message identifier code",
 *                  ),
 *                  @OA\Property(
 *                         property="message",
 *                         type="description of message",
 *                  ),
 *                  example={
 *                              {
 *                                  "code_message": "[UPDATED]",
 *                                  "message": "saucer updated"
 *                              },
 *                     }
 *                 ),
 *             )
 *          }
 *     ),
 *     @OA\Response(
 *         response=400,
 *         description="Exist conflict whit the request, please check the errors and messages",
 *         content={
 *                  @OA\MediaType(
 *                      mediaType="Success",
 *                      example="false",
 *                  ),
 *                 @OA\MediaType(
 *                      mediaType="Data",
 *                      example={
 *                      }
 *                  ),
 *                 
 *             @OA\MediaType(
 *                 mediaType="Errors",
 *                 @OA\Schema(
 *                      title="errors",
 *                      @OA\Property(
 *                         property="code_message",
 *                         type="message identifier code",
 *                  ),
 *                  @OA\Property(
 *                         property="field",
 *                         type="field presenting anomaly",
 *                  ),
 *                  @OA\Property(
 *                         property="message",
 *                         type="description of what's going on with the field",
 *                  ),
 *                  example={
 *                              {
 *                                  "code_message": "ERR_ID",
 *                                  "field": "saucer_id",
 *                                  "message": "saucer_id does not exist."
 *                              },
 *                              {
 *                                  "code_message": "ERR_REQUIRED",
 *                                  "field": "name",
 *                                  "message": "The name field is required."
 *                              },
 *                              {
 *                                  "code_message": "ERR_EXISTS",
 *                                  "field": "saurce_category_id",
 *                                  "message": "The selected saurce category id is invalid."
 *                              },
 *                              {
 *                                  "code_message": "ERR_ARRAY",
 *                                  "field": "images",
 *                                  "message": "The images must be an array."
 *                              },
 *                     }
 *                 ),
 *             ),
 *               @OA\MediaType(
 *                 mediaType="Messages",
 *                 @OA\Schema(
 *                      title="errors",
 *                      @OA\Property(
 *                         property="code_message",
 *                         type="message identifier code",
 *                  ),
 *                  @OA\Property(
 *                         property="message",
 *                         type="description of message",
 *                  ),
 *                  example={
 *                              {
 *                                  "code_message": "[ERR_CHECK_DATA]",
 *                                  "message": "The form has errors whit the inputs"
 *                              },
 *                     }
 *                 ),
 *             )
 *         }
 *     )
 * )
 * @OA\Delete(
 *     path="/api/v1/category_saucer/delete/{saucer_category_id}",
 *     tags={"SaucerCategory"},
 *     summary="Delete saucer cateegory with id.",
 *     description="Allows you to delete saucer category",
 *     operationId="deleteSaucerCategory",
 *     @OA\Parameter(
 *         name="Accept",
 *         in="header",
 *         description="application/json",
 *         required=true,
 *         explode=true,
 *     ),
 *     @OA\Parameter(
 *         name="Content-Type",
 *         in="header",
 *         description="application/x-www-form-urlencoded",
 *         required=true,
 *         explode=true,
 *     ), 
 *     @OA\Parameter(
 *         name="Authorization",
 *         in="header",
 *         description="Bearer [token]",
 *         required=true,
 *         explode=true,
 *     ),
 *     security={
 *       {"passport": {}},
 *     },
 *     @OA\Parameter(
 *         name="saucer_category_id",
 *         in="path",
 *         description="id of the saucer category",
 *         required=true,
 *         explode=true,
 *    ),
 *    @OA\Response(
 *         response=200,
 *         description="Saucer deleted successfully",
 *         content={
 *             @OA\MediaType(
 *                  mediaType="Success",
 *                  example="true",
 *               ),
 *                 @OA\MediaType(
 *                      mediaType="Data",
 *                      example="TODO"
 *                      
 *                  ),
 *               @OA\MediaType(
 *                      mediaType="Errors",
 *                      example={}
 *                      
 *               ),
 *               @OA\MediaType(
 *                 mediaType="Messages",
 *                 @OA\Schema(
 *                      @OA\Property(
 *                         property="code_message",
 *                         type="message identifier code",
 *                  ),
 *                  @OA\Property(
 *                         property="message",
 *                         type="description of message",
 *                  ),
 *                  example={
 *                              {
 *                                  "code_message": "[DELETED]",
 *                                  "message": "saucer deleted"
 *                              },
 *                     }
 *                 ),
 *             )
 *          }
 *     ),
 *     @OA\Response(
 *         response=400,
 *         description="Exist conflict whit the request, please check the errors and messages",
 *         content={
 *                  @OA\MediaType(
 *                      mediaType="Success",
 *                      example="false",
 *                  ),
 *                 @OA\MediaType(
 *                      mediaType="Data",
 *                      example={
 *                      }
 *                  ),
 *                 
 *             @OA\MediaType(
 *                 mediaType="Errors",
 *                 @OA\Schema(
 *                      title="errors",
 *                      @OA\Property(
 *                         property="code_message",
 *                         type="message identifier code",
 *                  ),
 *                  @OA\Property(
 *                         property="field",
 *                         type="field presenting anomaly",
 *                  ),
 *                  @OA\Property(
 *                         property="message",
 *                         type="description of what's going on with the field",
 *                  ),
 *                  example={
 *                              {
 *                                  "code_message": "ERR_ID",
 *                                  "field": "saucer_id",
 *                                  "message": "saucer_id does not exist."
 *                              },
 *                     }
 *                 ),
 *             ),
 *               @OA\MediaType(
 *                 mediaType="Messages",
 *                 @OA\Schema(
 *                      title="errors",
 *                      @OA\Property(
 *                         property="code_message",
 *                         type="message identifier code",
 *                  ),
 *                  @OA\Property(
 *                         property="message",
 *                         type="description of message",
 *                  ),
 *                  example={
 *                              {
 *                                  "code_message": "[ERR_CHECK_DATA]",
 *                                  "message": "The form has errors whit the inputs"
 *                              },
 *                     }
 *                 ),
 *             )
 *         }
 *     )
 * )
 */