<?php

/**
 *  @OA\Post(
 *     path="/api/v1/shopping_cart/add_item",
 *     tags={"Shopping Cart"},
 *     summary="add item",
 *     description="add item to shopping cart",
 *     operationId="addItem",
 *
 *     @OA\Parameter(
 *         name="Accept",
 *         in="header",
 *         description="application/json",
 *         required=true,
 *         explode=true,
 *     ),
 *     @OA\Parameter(
 *         name="Content-Type",
 *         in="header",
 *         description="application/x-www-form-urlencoded",
 *         required=true,
 *         explode=true,
 *     ),
 *     @OA\Parameter(
 *         name="Authorization",
 *         in="header",
 *         description="Bearer [token]",
 *         required=true,
 *         explode=true,
 *     ),
 *     security={
 *       {"passport": {}},
 *     },
 *     @OA\Parameter(
 *         name="product_id",
 *         in="query",
 *         description="product id",
 *         required=true,
 *         explode=true,
 *    ),
 *     @OA\Parameter(
 *         name="quantity",
 *         in="query",
 *         description="quantity",
 *         required=true,
 *         explode=true,
 *    ),
 *    @OA\Response(
 *         response=200,
 *         description="added resource",
 *         content={
 *             @OA\MediaType(
 *                  mediaType="Success",
 *                  example="true",
 *               ),
 *                 @OA\MediaType(
 *                      mediaType="Data",
 *                      example="TODO"
 *
 *                  ),
 *               @OA\MediaType(
 *                      mediaType="Errors",
 *                      example={}
 *
 *               ),
 *               @OA\MediaType(
 *                 mediaType="Messages",
 *                 @OA\Schema(
 *                      @OA\Property(
 *                         property="code_message",
 *                         type="message identifier code",
 *                  ),
 *                  @OA\Property(
 *                         property="message",
 *                         type="description of message",
 *                  ),
 *                  example={
 *                              {
 *                                  "code_message": "ADDED",
 *                                  "message": "resource added"
 *                              },
 *                     }
 *                 ),
 *             )
 *          }
 *     ),
 *     @OA\Response(
 *         response=400,
 *         description="Exist conflict whit the request, please check the errors and messages",
 *         content={
 *                  @OA\MediaType(
 *                      mediaType="Success",
 *                      example="false",
 *                  ),
 *                 @OA\MediaType(
 *                      mediaType="Data",
 *                      example={
 *                      }
 *                  ),
 *
 *             @OA\MediaType(
 *                 mediaType="Errors",
 *                 @OA\Schema(
 *                      title="errors",
 *                      @OA\Property(
 *                         property="code_message",
 *                         type="message identifier code",
 *                  ),
 *                  @OA\Property(
 *                         property="field",
 *                         type="field presenting anomaly",
 *                  ),
 *                  @OA\Property(
 *                         property="message",
 *                         type="description of what's going on with the field",
 *                  ),
 *                  example={
 *                              {
 *                                  "code_message": "ERR_REQUIRED",
 *                                  "field": "quantity",
 *                                  "message": "The quantity id field is required."
 *                              },
 *                              {
 *                                  "code_message": "ERR_EXISTS",
 *                                  "field": "saurce_id",
 *                                  "message": "The selected saurce id is invalid."
 *                              },
 *
 *                     }
 *                 ),
 *             ),
 *               @OA\MediaType(
 *                 mediaType="Messages",
 *                 @OA\Schema(
 *                      title="errors",
 *                      @OA\Property(
 *                         property="code_message",
 *                         type="message identifier code",
 *                  ),
 *                  @OA\Property(
 *                         property="message",
 *                         type="description of message",
 *                  ),
 *                  example={
 *                              {
 *                                  "code_message": "[ERR_CHECK_DATA]",
 *                                  "message": "The form has errors whit the inputs"
 *                              },
 *                     }
 *                 ),
 *             )
 *         }
 *     )
 * )
 *   @OA\Get(
 *     path="/api/v1/shopping_cart/show/{shopping_cart_id}",
 *     tags={"Shopping Cart"},
 *     summary="show cart",
 *     description="allow show cart",
 *     operationId="addItem",
 *
 *     @OA\Parameter(
 *         name="Accept",
 *         in="header",
 *         description="application/json",
 *         required=true,
 *         explode=true,
 *     ),
 *     @OA\Parameter(
 *         name="Content-Type",
 *         in="header",
 *         description="application/x-www-form-urlencoded",
 *         required=true,
 *         explode=true,
 *     ),
 *     @OA\Parameter(
 *         name="Authorization",
 *         in="header",
 *         description="Bearer [token]",
 *         required=true,
 *         explode=true,
 *     ),
 *     security={
 *       {"passport": {}},
 *     },
 *     @OA\Parameter(
 *         name="shopping_cart_id",
 *         in="path",
 *         description="shopping cart id",
 *         required=true,
 *         explode=true,
 *    ),
 *    @OA\Response(
 *         response=200,
 *         description="resource was found",
 *         content={
 *             @OA\MediaType(
 *                  mediaType="Success",
 *                  example="true",
 *               ),
 *                 @OA\MediaType(
 *                      mediaType="Data",
 *                      example="TODO"
 *
 *                  ),
 *               @OA\MediaType(
 *                      mediaType="Errors",
 *                      example={}
 *
 *               ),
 *               @OA\MediaType(
 *                 mediaType="Messages",
 *                 @OA\Schema(
 *                      @OA\Property(
 *                         property="code_message",
 *                         type="message identifier code",
 *                  ),
 *                  @OA\Property(
 *                         property="message",
 *                         type="description of message",
 *                  ),
 *                  example={
 *                              {
 *                                  "code_message": "FOUND",
 *                                  "message": "resource found"
 *                              },
 *                     }
 *                 ),
 *             )
 *          }
 *     ),
 * )
 *   @OA\Delete(
 *     path="/api/v1/shopping_cart/delete_item/{item_id}",
 *     tags={"Shopping Cart"},
 *     summary="delete item",
 *     description="allow delete item from shopping cart",
 *     operationId="addItem",
 *
 *     @OA\Parameter(
 *         name="Accept",
 *         in="header",
 *         description="application/json",
 *         required=true,
 *         explode=true,
 *     ),
 *     @OA\Parameter(
 *         name="Content-Type",
 *         in="header",
 *         description="application/x-www-form-urlencoded",
 *         required=true,
 *         explode=true,
 *     ),
 *     @OA\Parameter(
 *         name="Authorization",
 *         in="header",
 *         description="Bearer [token]",
 *         required=true,
 *         explode=true,
 *     ),
 *     security={
 *       {"passport": {}},
 *     },
 *     @OA\Parameter(
 *         name="item_id",
 *         in="path",
 *         description="item id",
 *         required=true,
 *         explode=true,
 *    ),
 *    @OA\Response(
 *         response=200,
 *         description="resource was removed",
 *         content={
 *             @OA\MediaType(
 *                  mediaType="Success",
 *                  example="true",
 *               ),
 *                 @OA\MediaType(
 *                      mediaType="Data",
 *                      example="TODO"
 *
 *                  ),
 *               @OA\MediaType(
 *                      mediaType="Errors",
 *                      example={}
 *
 *               ),
 *               @OA\MediaType(
 *                 mediaType="Messages",
 *                 @OA\Schema(
 *                      @OA\Property(
 *                         property="code_message",
 *                         type="message identifier code",
 *                  ),
 *                  @OA\Property(
 *                         property="message",
 *                         type="description of message",
 *                  ),
 *                  example={
 *                              {
 *                                  "code_message": "DELETED",
 *                                  "message": "resource deleted"
 *                              },
 *                     }
 *                 ),
 *             )
 *          }
 *     ),
 * )
 */

