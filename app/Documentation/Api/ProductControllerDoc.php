<?php
/**
 *  @OA\Post(
 *     path="/api/v1/product/register",
 *     tags={"Product"},
 *     summary="Register a new product",
 *     description="Allows you to register a product",
 *     operationId="regProduct",
 *
 *     @OA\Parameter(
 *         name="Accept",
 *         in="header",
 *         description="application/json",
 *         required=true,
 *         explode=true,
 *     ),
 *     @OA\Parameter(
 *         name="Content-Type",
 *         in="header",
 *         description="application/x-www-form-urlencoded",
 *         required=true,
 *         explode=true,
 *     ),
 *     @OA\Parameter(
 *         name="Authorization",
 *         in="header",
 *         description="Bearer [token]",
 *         required=true,
 *         explode=true,
 *     ),
 *     security={
 *       {"passport": {}},
 *     },
 *     @OA\Parameter(
 *         name="name",
 *         in="query",
 *         description="product name",
 *         required=true,
 *         explode=true,
 *    ),
 *     @OA\Parameter(
 *         name="description",
 *         in="query",
 *         description="product description",
 *         required=true,
 *         explode=true,
 *    ),
 *    @OA\Parameter(
 *         name="cost",
 *         in="query",
 *         description="product cost",
 *         required=true,
 *         explode=true,
 *    ),
 *    @OA\Parameter(
 *         name="stock",
 *         in="query",
 *         description="product quantity",
 *         required=true,
 *         explode=true,
 *    ),
  *    @OA\Parameter(
 *         name="restaurant_id",
 *         in="query",
 *         description="id of the restaurant to which the product belongs",
 *         required=true,
 *         explode=true,
 *    ),
 *   @OA\Parameter(
 *         name="categories_product_id",
 *         in="query",
 *         description="id of the category to which the product belongs",
 *         required=true,
 *         explode=true,
 *    ),
 *   @OA\Parameter(
 *         name="image",
 *         in="query",
 *         description="url image",
 *         required=true,
 *         explode=true,
 *    ),
 *    @OA\Response(
 *         response=200,
 *         description="product registered",
 *         content={
 *             @OA\MediaType(
 *                  mediaType="Success",
 *                  example="true",
 *               ),
 *                 @OA\MediaType(
 *                      mediaType="Data",
 *                      example="TODO"
 *                      
 *                  ),
 *               @OA\MediaType(
 *                      mediaType="Errors",
 *                      example={}
 *                      
 *               ),
 *               @OA\MediaType(
 *                 mediaType="Messages",
 *                 @OA\Schema(
 *                      @OA\Property(
 *                         property="code_message",
 *                         type="message identifier code",
 *                  ),
 *                  @OA\Property(
 *                         property="message",
 *                         type="description of message",
 *                  ),
 *                  example={
 *                              {
 *                                  "code_message": "[REGISTERED]",
 *                                  "message": "resource registered"
 *                              },
 *                     }
 *                 ),
 *             )
 *          }
 *     ),
 *     @OA\Response(
 *         response=400,
 *         description="Exist conflict whit the request, please check the errors and messages",
 *         content={
 *                  @OA\MediaType(
 *                      mediaType="Success",
 *                      example="false",
 *                  ),
 *                 @OA\MediaType(
 *                      mediaType="Data",
 *                      example={
 *                      }
 *                  ),
 *                 
 *             @OA\MediaType(
 *                 mediaType="Errors",
 *                 @OA\Schema(
 *                      title="errors",
 *                      @OA\Property(
 *                         property="code_message",
 *                         type="message identifier code",
 *                  ),
 *                  @OA\Property(
 *                         property="field",
 *                         type="field presenting anomaly",
 *                  ),
 *                  @OA\Property(
 *                         property="message",
 *                         type="description of what's going on with the field",
 *                  ),
 *                  example={
 *                              {
 *                                  "code_message": "ERR_REQUIRED",
 *                                  "field": "name",
 *                                  "message": "The name field is required."
 *                              },
 *                              {
 *                                  "code_message": "ERR_EXISTS",
 *                                  "field": "categories_product_id",
 *                                  "message": "The selected product category id is invalid."
 *                              },
 *                              {
 *                                  "code_message": "ERR_ARRAY",
 *                                  "field": "images",
 *                                  "message": "The images must be an array."
 *                              },
 *                     }
 *                 ),
 *             ),
 *               @OA\MediaType(
 *                 mediaType="Messages",
 *                 @OA\Schema(
 *                      title="errors",
 *                      @OA\Property(
 *                         property="code_message",
 *                         type="message identifier code",
 *                  ),
 *                  @OA\Property(
 *                         property="message",
 *                         type="description of message",
 *                  ),
 *                  example={
 *                              {
 *                                  "code_message": "[ERR_CHECK_DATA]",
 *                                  "message": "The form has errors whit the inputs"
 *                              },
 *                     }
 *                 ),
 *             )
 *         }
 *     )
 * )
 * 
 * @OA\Get(
 *     path="/api/v1/product/list/{restaurant_id}",
 *     tags={"Product"},
 *     summary="List all the products in a restaurant.",
 *     description="Allows you to list  products from a restaurant.",
 *     operationId="listProducts",
 *     @OA\Parameter(
 *         name="Accept",
 *         in="header",
 *         description="application/json",
 *         required=true,
 *         explode=true,
 *     ),
 *     @OA\Parameter(
 *         name="Content-Type",
 *         in="header",
 *         description="application/x-www-form-urlencoded",
 *         required=true,
 *         explode=true,
 *     ), 
 *    @OA\Response(
 *         response=200,
 *         description="Products retrieved successfully",
 *         content={
 *             @OA\MediaType(
 *                  mediaType="Success",
 *                  example="true",
 *               ),
 *                 @OA\MediaType(
 *                      mediaType="Data",
 *                      example="TODO"
 *                      
 *                  ),
 *               @OA\MediaType(
 *                      mediaType="Errors",
 *                      example={}
 *                      
 *               ),
 *               @OA\MediaType(
 *                 mediaType="Messages",
 *                 @OA\Schema(
 *                      @OA\Property(
 *                         property="code_message",
 *                         type="message identifier code",
 *                  ),
 *                  @OA\Property(
 *                         property="message",
 *                         type="description of message",
 *                  ),
 *                  example={
 *                              {
 *                                  "code_message": "[SUCCESS]",
 *                                  "message": "Products found"
 *                              },
 *                     }
 *                 ),
 *             )
 *          }
 *     ),
 * )
 * 
 * @OA\Get(
 *     path="/api/v1/product/show/{restaurant_id}/{product_id}",
 *     tags={"Product"},
 *     summary="Get product with id.",
 *     description="Allows you to get product from a restaurant.",
 *     operationId="getProductId",
 *
 *     @OA\Parameter(
 *         name="Accept",
 *         in="header",
 *         description="application/json",
 *         required=true,
 *         explode=true,
 *     ),
 *     @OA\Parameter(
 *         name="Content-Type",
 *         in="header",
 *         description="application/x-www-form-urlencoded",
 *         required=true,
 *         explode=true,
 *     ), 
 *     @OA\Parameter(
 *         name="product_id",
 *         in="path",
 *         description="id of the product",
 *         required=true,
 *         explode=true,
 *    ),
 *    @OA\Response(
 *         response=200,
 *         description="Product retrieved successfully",
 *         content={
 *             @OA\MediaType(
 *                  mediaType="Success",
 *                  example="true",
 *               ),
 *                 @OA\MediaType(
 *                      mediaType="Data",
 *                      example="TODO"
 *                      
 *                  ),
 *               @OA\MediaType(
 *                      mediaType="Errors",
 *                      example={}
 *                      
 *               ),
 *               @OA\MediaType(
 *                 mediaType="Messages",
 *                 @OA\Schema(
 *                      @OA\Property(
 *                         property="code_message",
 *                         type="message identifier code",
 *                  ),
 *                  @OA\Property(
 *                         property="message",
 *                         type="description of message",
 *                  ),
 *                  example={
 *                              {
 *                                  "code_message": "[SUCCESS]",
 *                                  "message": "Product found"
 *                              },
 *                     }
 *                 ),
 *             )
 *          }
 *     ),
 * )
 * 
 * @OA\Get(
 *     path="/api/v1/product/show_by_name/{restauran_id}/{product_name}",
 *     tags={"Product"},
 *     summary="Get product with name.",
 *     description="Allows you to get product from a restaurant.",
 *     operationId="getProductName",
 *
 *     @OA\Parameter(
 *         name="Accept",
 *         in="header",
 *         description="application/json",
 *         required=true,
 *         explode=true,
 *     ),
 *     @OA\Parameter(
 *         name="Content-Type",
 *         in="header",
 *         description="application/x-www-form-urlencoded",
 *         required=true,
 *         explode=true,
 *     ), 
 *     @OA\Parameter(
 *         name="product_name",
 *         in="path",
 *         description="product name",
 *         required=true,
 *         explode=true,
 *    ),
 *    @OA\Response(
 *         response=200,
 *         description="Products retrieved successfully",
 *         content={
 *             @OA\MediaType(
 *                  mediaType="Success",
 *                  example="true",
 *               ),
 *                 @OA\MediaType(
 *                      mediaType="Data",
 *                      example="TODO"
 *                      
 *                  ),
 *               @OA\MediaType(
 *                      mediaType="Errors",
 *                      example={}
 *                      
 *               ),
 *               @OA\MediaType(
 *                 mediaType="Messages",
 *                 @OA\Schema(
 *                      @OA\Property(
 *                         property="code_message",
 *                         type="message identifier code",
 *                  ),
 *                  @OA\Property(
 *                         property="message",
 *                         type="description of message",
 *                  ),
 *                  example={
 *                              {
 *                                  "code_message": "[SUCCESS]",
 *                                  "message": "Products found"
 *                              },
 *                     }
 *                 ),
 *             )
 *          }
 *     ),
 * )
 * 
 * @OA\Put(
 *     path="/api/v1/product/update/",
 *     tags={"Product"},
 *     summary="Update product with id.",
 *     description="Allows you to update product from a restaurant.",
 *     operationId="updateSaucer",
 *
*     @OA\Parameter(
 *         name="Accept",
 *         in="header",
 *         description="application/json",
 *         required=true,
 *         explode=true,
 *     ),
 *     @OA\Parameter(
 *         name="Content-Type",
 *         in="header",
 *         description="application/x-www-form-urlencoded",
 *         required=true,
 *         explode=true,
 *     ),
 *     @OA\Parameter(
 *         name="Authorization",
 *         in="header",
 *         description="Bearer [token]",
 *         required=true,
 *         explode=true,
 *     ),
 *     security={
 *       {"passport": {}},
 *     },
 *     @OA\Parameter(
 *         name="product_id",
 *         in="query",
 *         description="id of the product",
 *         required=true,
 *         explode=true,
 *     ),
 *     @OA\Parameter(
 *         name="name",
 *         in="query",
 *         description="product name",
 *         required=false,
 *         explode=true,
 *    ),
 *     @OA\Parameter(
 *         name="description",
 *         in="query",
 *         description="product description",
 *         required=false,
 *         explode=true,
 *    ),
 *    @OA\Parameter(
 *         name="cost",
 *         in="query",
 *         description="product cost",
 *         required=false,
 *         explode=true,
 *    ),
 *    @OA\Parameter(
 *         name="stock",
 *         in="query",
 *         description="product stock",
 *         required=false,
 *         explode=true,
 *    ),
 *   @OA\Parameter(
 *         name="categories_product_id",
 *         in="query",
 *         description="id of the category to which the product belongs",
 *         required=false,
 *         explode=true,
 *    ),
 *   @OA\Parameter(
 *         name="image",
 *         in="query",
 *         description="url image",
 *         required=false,
 *         explode=true,
 *    ),
 *    @OA\Response(
 *         response=200,
 *         description="product updated successfully",
 *         content={
 *             @OA\MediaType(
 *                  mediaType="Success",
 *                  example="true",
 *               ),
 *                 @OA\MediaType(
 *                      mediaType="Data",
 *                      example="TODO"
 *                      
 *                  ),
 *               @OA\MediaType(
 *                      mediaType="Errors",
 *                      example={}
 *                      
 *               ),
 *               @OA\MediaType(
 *                 mediaType="Messages",
 *                 @OA\Schema(
 *                      @OA\Property(
 *                         property="code_message",
 *                         type="message identifier code",
 *                  ),
 *                  @OA\Property(
 *                         property="message",
 *                         type="description of message",
 *                  ),
 *                  example={
 *                              {
 *                                  "code_message": "[UPDATED]",
 *                                  "message": "product updated"
 *                              },
 *                     }
 *                 ),
 *             )
 *          }
 *     ),
 *     @OA\Response(
 *         response=400,
 *         description="Exist conflict whit the request, please check the errors and messages",
 *         content={
 *                  @OA\MediaType(
 *                      mediaType="Success",
 *                      example="false",
 *                  ),
 *                 @OA\MediaType(
 *                      mediaType="Data",
 *                      example={
 *                      }
 *                  ),
 *                 
 *             @OA\MediaType(
 *                 mediaType="Errors",
 *                 @OA\Schema(
 *                      title="errors",
 *                      @OA\Property(
 *                         property="code_message",
 *                         type="message identifier code",
 *                  ),
 *                  @OA\Property(
 *                         property="field",
 *                         type="field presenting anomaly",
 *                  ),
 *                  @OA\Property(
 *                         property="message",
 *                         type="description of what's going on with the field",
 *                  ),
 *                  example={
 *                              {
 *                                  "code_message": "ERR_ID",
 *                                  "field": "product_id",
 *                                  "message": "product_id does not exist."
 *                              },
 *                              {
 *                                  "code_message": "ERR_REQUIRED",
 *                                  "field": "name",
 *                                  "message": "The name field is required."
 *                              },
 *                              {
 *                                  "code_message": "ERR_EXISTS",
 *                                  "field": "categories_product_id",
 *                                  "message": "The selected product category id is invalid."
 *                              },
 *                              {
 *                                  "code_message": "ERR_ARRAY",
 *                                  "field": "images",
 *                                  "message": "The images must be an array."
 *                              },
 *                     }
 *                 ),
 *             ),
 *               @OA\MediaType(
 *                 mediaType="Messages",
 *                 @OA\Schema(
 *                      title="errors",
 *                      @OA\Property(
 *                         property="code_message",
 *                         type="message identifier code",
 *                  ),
 *                  @OA\Property(
 *                         property="message",
 *                         type="description of message",
 *                  ),
 *                  example={
 *                              {
 *                                  "code_message": "[ERR_CHECK_DATA]",
 *                                  "message": "The form has errors whit the inputs"
 *                              },
 *                     }
 *                 ),
 *             )
 *         }
 *     )
 * )
 * @OA\Delete(
 *     path="/api/v1/product/delete/{restaurant_id}/{product_id}",
 *     tags={"Product"},
 *     summary="Delete product with id.",
 *     description="Allows you to delete product from a restaurant.",
 *     operationId="deleteProduct",
 *
 *     @OA\Parameter(
 *         name="Accept",
 *         in="header",
 *         description="application/json",
 *         required=true,
 *         explode=true,
 *     ),
 *     @OA\Parameter(
 *         name="Content-Type",
 *         in="header",
 *         description="application/x-www-form-urlencoded",
 *         required=true,
 *         explode=true,
 *     ), 
 *     @OA\Parameter(
 *         name="Authorization",
 *         in="header",
 *         description="Bearer [token]",
 *         required=true,
 *         explode=true,
 *     ),
 *     security={
 *       {"passport": {}},
 *     },
 *     @OA\Parameter(
 *         name="product_id",
 *         in="path",
 *         description="id of the product",
 *         required=true,
 *         explode=true,
 *    ),
 *    @OA\Response(
 *         response=200,
 *         description="Product deleted successfully",
 *         content={
 *             @OA\MediaType(
 *                  mediaType="Success",
 *                  example="true",
 *               ),
 *                 @OA\MediaType(
 *                      mediaType="Data",
 *                      example="TODO"
 *                      
 *                  ),
 *               @OA\MediaType(
 *                      mediaType="Errors",
 *                      example={}
 *                      
 *               ),
 *               @OA\MediaType(
 *                 mediaType="Messages",
 *                 @OA\Schema(
 *                      @OA\Property(
 *                         property="code_message",
 *                         type="message identifier code",
 *                  ),
 *                  @OA\Property(
 *                         property="message",
 *                         type="description of message",
 *                  ),
 *                  example={
 *                              {
 *                                  "code_message": "[DELETED]",
 *                                  "message": "product deleted"
 *                              },
 *                     }
 *                 ),
 *             )
 *          }
 *     ),
 * )
 
 */