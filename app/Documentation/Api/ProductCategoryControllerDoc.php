<?php
/**
 *  @OA\Post(
 *     path="/api/v1/product_saucer/register",
 *     tags={"ProductCategory"},
 *     summary="Register a new product category",
 *     description="Allows you to register a product category",
 *     operationId="regProductCategory",
 *
 *     @OA\Parameter(
 *         name="Accept",
 *         in="header",
 *         description="application/json",
 *         required=true,
 *         explode=true,
 *     ),
 *     @OA\Parameter(
 *         name="Content-Type",
 *         in="header",
 *         description="application/x-www-form-urlencoded",
 *         required=true,
 *         explode=true,
 *     ),
 *    @OA\Parameter(
 *         name="Authorization",
 *         in="header",
 *         description="Bearer [token]",
 *         required=true,
 *         explode=true,
 *     ),
 *     security={
 *       {"passport": {}},
 *     },
 *     @OA\Parameter(
 *         name="name",
 *         in="query",
 *         description="product category name",
 *         required=true,
 *         explode=true,
 *    ), 
 *    @OA\Response(
 *         response=200,
 *         description="saucer registered",
 *         content={
 *             @OA\MediaType(
 *                  mediaType="Success",
 *                  example="true",
 *               ),
 *                 @OA\MediaType(
 *                      mediaType="Data",
 *                      example="TODO"
 *                      
 *                  ),
 *               @OA\MediaType(
 *                      mediaType="Errors",
 *                      example={}
 *                      
 *               ),
 *               @OA\MediaType(
 *                 mediaType="Messages",
 *                 @OA\Schema(
 *                      @OA\Property(
 *                         property="code_message",
 *                         type="message identifier code",
 *                  ),
 *                  @OA\Property(
 *                         property="message",
 *                         type="description of message",
 *                  ),
 *                  example={
 *                              {
 *                                  "code_message": "[REGISTERED]",
 *                                  "message": "resource registered"
 *                              },
 *                     }
 *                 ),
 *             )
 *          }
 *     ),
 *     @OA\Response(
 *         response=400,
 *         description="Exist conflict whit the request, please check the errors and messages",
 *         content={
 *                  @OA\MediaType(
 *                      mediaType="Success",
 *                      example="false",
 *                  ),
 *                 @OA\MediaType(
 *                      mediaType="Data",
 *                      example={
 *                      }
 *                  ),
 *                 
 *             @OA\MediaType(
 *                 mediaType="Errors",
 *                 @OA\Schema(
 *                      title="errors",
 *                      @OA\Property(
 *                         property="code_message",
 *                         type="message identifier code",
 *                  ),
 *                  @OA\Property(
 *                         property="field",
 *                         type="field presenting anomaly",
 *                  ),
 *                  @OA\Property(
 *                         property="message",
 *                         type="description of what's going on with the field",
 *                  ),
 *                  example={
 *                              {
 *                                  "code_message": "ERR_REQUIRED",
 *                                  "field": "name",
 *                                  "message": "The name field is required."
 *                              },
 *                              {
 *                                  "code_message": "ERR_EXISTS",
 *                                  "field": "saurce_category_id",
 *                                  "message": "The selected saurce category id is invalid."
 *                              },
 *                              {
 *                                  "code_message": "ERR_ARRAY",
 *                                  "field": "images",
 *                                  "message": "The images must be an array."
 *                              },
 *                     }
 *                 ),
 *             ),
 *               @OA\MediaType(
 *                 mediaType="Messages",
 *                 @OA\Schema(
 *                      title="errors",
 *                      @OA\Property(
 *                         property="code_message",
 *                         type="message identifier code",
 *                  ),
 *                  @OA\Property(
 *                         property="message",
 *                         type="description of message",
 *                  ),
 *                  example={
 *                              {
 *                                  "code_message": "[ERR_CHECK_DATA]",
 *                                  "message": "The form has errors whit the inputs"
 *                              },
 *                     }
 *                 ),
 *             )
 *         }
 *     )
 * )
 * 
 *  @OA\Get(
 *     path="/api/v1/category_product/list",
 *     tags={"ProductCategory"},
 *     summary="List all the categories products.",
 *     description="Allows you to list products categories",
 *     operationId="listCategoriesProducts",
 *
 *     @OA\Parameter(
 *         name="Accept",
 *         in="header",
 *         description="application/json",
 *         required=true,
 *         explode=true,
 *     ),
 *     @OA\Parameter(
 *         name="Content-Type",
 *         in="header",
 *         description="application/x-www-form-urlencoded",
 *         required=true,
 *         explode=true,
 *     ),  
 *    @OA\Response(
 *         response=200,
 *         description="resource was found",
 *         content={
 *             @OA\MediaType(
 *                  mediaType="Success",
 *                  example="true",
 *               ),
 *                 @OA\MediaType(
 *                      mediaType="Data",
 *                      example="TODO"
 *                      
 *                  ),
 *               @OA\MediaType(
 *                      mediaType="Errors",
 *                      example={}
 *                      
 *               ),
 *               @OA\MediaType(
 *                 mediaType="Messages",
 *                 @OA\Schema(
 *                      @OA\Property(
 *                         property="code_message",
 *                         type="message identifier code",
 *                  ),
 *                  @OA\Property(
 *                         property="message",
 *                         type="description of message",
 *                  ),
 *                  example={
 *                              {
 *                                  "code_message": "FOUND",
 *                                  "message": "resource found"
 *                              },
 *                     }
 *                 ),
 *             )
 *          }
 *     ),
 * )
 * 
 * @OA\Get(
 *     path="/api/v1/category_product/show/{product_category_id}",
 *     tags={"ProductCategory"},
 *     summary="Get product category with id.",
 *     description="Allows you to get product category",
 *     operationId="getProductCategoryId",
 *
 *     @OA\Parameter(
 *         name="Accept",
 *         in="header",
 *         description="application/json",
 *         required=true,
 *         explode=true,
 *     ),
 *     @OA\Parameter(
 *         name="Content-Type",
 *         in="header",
 *         description="application/x-www-form-urlencoded",
 *         required=true,
 *         explode=true,
 *     ), 
 *     @OA\Parameter(
 *         name="product_category_id",
 *         in="path",
 *         description="id of the product category",
 *         required=true,
 *         explode=true,
 *    ),
 *    @OA\Response(
 *         response=200,
 *         description="resource was found",
 *         content={
 *             @OA\MediaType(
 *                  mediaType="Success",
 *                  example="true",
 *               ),
 *                 @OA\MediaType(
 *                      mediaType="Data",
 *                      example="TODO"
 *                      
 *                  ),
 *               @OA\MediaType(
 *                      mediaType="Errors",
 *                      example={}
 *                      
 *               ),
 *               @OA\MediaType(
 *                 mediaType="Messages",
 *                 @OA\Schema(
 *                      @OA\Property(
 *                         property="code_message",
 *                         type="message identifier code",
 *                  ),
 *                  @OA\Property(
 *                         property="message",
 *                         type="description of message",
 *                  ),
 *                  example={
 *                              {
 *                                  "code_message": "FOUND",
 *                                  "message": "resource was found"
 *                              },
 *                     }
 *                 ),
 *             )
 *          }
 *     ),
 * )
 * 
 * @OA\Get(
 *     path="/api/v1/category_product/show_by_name/{product_category_name}",
 *     tags={"ProductCategory"},
 *     summary="Get saucer category with name.",
 *     description="Allows you to get product category.",
 *     operationId="getProductCategoryName",
 *
 *     @OA\Parameter(
 *         name="Accept",
 *         in="header",
 *         description="application/json",
 *         required=true,
 *         explode=true,
 *     ),
 *     @OA\Parameter(
 *         name="Content-Type",
 *         in="header",
 *         description="application/x-www-form-urlencoded",
 *         required=true,
 *         explode=true,
 *     ), 
 *     @OA\Parameter(
 *         name="product_category_name",
 *         in="path",
 *         description="product name",
 *         required=true,
 *         explode=true,
 *    ),
 *    @OA\Response(
 *         response=200,
 *         description="Products retrieved successfully",
 *         content={
 *             @OA\MediaType(
 *                  mediaType="Success",
 *                  example="true",
 *               ),
 *                 @OA\MediaType(
 *                      mediaType="Data",
 *                      example="TODO"
 *                      
 *                  ),
 *               @OA\MediaType(
 *                      mediaType="Errors",
 *                      example={}
 *                      
 *               ),
 *               @OA\MediaType(
 *                 mediaType="Messages",
 *                 @OA\Schema(
 *                      @OA\Property(
 *                         property="code_message",
 *                         type="message identifier code",
 *                  ),
 *                  @OA\Property(
 *                         property="message",
 *                         type="description of message",
 *                  ),
 *                  example={
 *                              {
 *                                  "code_message": "[SUCCESS]",
 *                                  "message": "Saucers found"
 *                              },
 *                     }
 *                 ),
 *             )
 *          }
 *     ),
 * )
 * 
 * @OA\Put(
 *     path="/api/v1/category_product/update",
 *     tags={"ProductCategory"},
 *     summary="Update product categry with id.",
 *     description="Allows you to update product category",
 *     operationId="updateProductCategory",
 *
*     @OA\Parameter(
 *         name="Accept",
 *         in="header",
 *         description="application/json",
 *         required=true,
 *         explode=true,
 *     ),
 *     @OA\Parameter(
 *         name="Content-Type",
 *         in="header",
 *         description="application/x-www-form-urlencoded",
 *         required=true,
 *         explode=true,
 *     ),
 *      @OA\Parameter(
 *         name="Authorization",
 *         in="header",
 *         description="Bearer [token]",
 *         required=true,
 *         explode=true,
 *     ),
 *     security={
 *       {"passport": {}},
 *     },
 *     @OA\Parameter(
 *         name="product_category_id",
 *         in="query",
 *         description="id of the product category",
 *         required=true,
 *         explode=true,
 *     ), 
 *     @OA\Parameter(
 *         name="name",
 *         in="query",
 *         description="product category name",
 *         required=false,
 *         explode=true,
 *    ),
 *    @OA\Response(
 *         response=200,
 *         description="saucer updated successfully",
 *         content={
 *             @OA\MediaType(
 *                  mediaType="Success",
 *                  example="true",
 *               ),
 *                 @OA\MediaType(
 *                      mediaType="Data",
 *                      example="TODO"
 *                      
 *                  ),
 *               @OA\MediaType(
 *                      mediaType="Errors",
 *                      example={}
 *                      
 *               ),
 *               @OA\MediaType(
 *                 mediaType="Messages",
 *                 @OA\Schema(
 *                      @OA\Property(
 *                         property="code_message",
 *                         type="message identifier code",
 *                  ),
 *                  @OA\Property(
 *                         property="message",
 *                         type="description of message",
 *                  ),
 *                  example={
 *                              {
 *                                  "code_message": "[UPDATED]",
 *                                  "message": "saucer updated"
 *                              },
 *                     }
 *                 ),
 *             )
 *          }
 *     ),
 *     @OA\Response(
 *         response=400,
 *         description="Exist conflict whit the request, please check the errors and messages",
 *         content={
 *                  @OA\MediaType(
 *                      mediaType="Success",
 *                      example="false",
 *                  ),
 *                 @OA\MediaType(
 *                      mediaType="Data",
 *                      example={
 *                      }
 *                  ),
 *                 
 *             @OA\MediaType(
 *                 mediaType="Errors",
 *                 @OA\Schema(
 *                      title="errors",
 *                      @OA\Property(
 *                         property="code_message",
 *                         type="message identifier code",
 *                  ),
 *                  @OA\Property(
 *                         property="field",
 *                         type="field presenting anomaly",
 *                  ),
 *                  @OA\Property(
 *                         property="message",
 *                         type="description of what's going on with the field",
 *                  ),
 *                  example={
 *                              {
 *                                  "code_message": "ERR_ID",
 *                                  "field": "saucer_id",
 *                                  "message": "saucer_id does not exist."
 *                              },
 *                              {
 *                                  "code_message": "ERR_REQUIRED",
 *                                  "field": "name",
 *                                  "message": "The name field is required."
 *                              },
 *                              {
 *                                  "code_message": "ERR_EXISTS",
 *                                  "field": "saurce_category_id",
 *                                  "message": "The selected saurce category id is invalid."
 *                              },
 *                              {
 *                                  "code_message": "ERR_ARRAY",
 *                                  "field": "images",
 *                                  "message": "The images must be an array."
 *                              },
 *                     }
 *                 ),
 *             ),
 *               @OA\MediaType(
 *                 mediaType="Messages",
 *                 @OA\Schema(
 *                      title="errors",
 *                      @OA\Property(
 *                         property="code_message",
 *                         type="message identifier code",
 *                  ),
 *                  @OA\Property(
 *                         property="message",
 *                         type="description of message",
 *                  ),
 *                  example={
 *                              {
 *                                  "code_message": "[ERR_CHECK_DATA]",
 *                                  "message": "The form has errors whit the inputs"
 *                              },
 *                     }
 *                 ),
 *             )
 *         }
 *     )
 * )
 * @OA\Delete(
 *     path="/api/v1/category_product/delete/{product_category_id}",
 *     tags={"ProductCategory"},
 *     summary="Delete product category with id.",
 *     description="Allows you to delete product category",
 *     operationId="deleteProductCategory",
 *     @OA\Parameter(
 *         name="Accept",
 *         in="header",
 *         description="application/json",
 *         required=true,
 *         explode=true,
 *     ),
 *     @OA\Parameter(
 *         name="Content-Type",
 *         in="header",
 *         description="application/x-www-form-urlencoded",
 *         required=true,
 *         explode=true,
 *     ), 
 *      @OA\Parameter(
 *         name="Authorization",
 *         in="header",
 *         description="Bearer [token]",
 *         required=true,
 *         explode=true,
 *     ),
 *     security={
 *       {"passport": {}},
 *     },
 *     @OA\Parameter(
 *         name="product_category_id",
 *         in="path",
 *         description="id of the product category",
 *         required=true,
 *         explode=true,
 *    ),
 *    @OA\Response(
 *         response=200,
 *         description="product deleted successfully",
 *         content={
 *             @OA\MediaType(
 *                  mediaType="Success",
 *                  example="true",
 *               ),
 *                 @OA\MediaType(
 *                      mediaType="Data",
 *                      example="TODO"
 *                      
 *                  ),
 *               @OA\MediaType(
 *                      mediaType="Errors",
 *                      example={}
 *                      
 *               ),
 *               @OA\MediaType(
 *                 mediaType="Messages",
 *                 @OA\Schema(
 *                      @OA\Property(
 *                         property="code_message",
 *                         type="message identifier code",
 *                  ),
 *                  @OA\Property(
 *                         property="message",
 *                         type="description of message",
 *                  ),
 *                  example={
 *                              {
 *                                  "code_message": "[DELETED]",
 *                                  "message": "saucer deleted"
 *                              },
 *                     }
 *                 ),
 *             )
 *          }
 *     ),
 *     @OA\Response(
 *         response=400,
 *         description="Exist conflict whit the request, please check the errors and messages",
 *         content={
 *                  @OA\MediaType(
 *                      mediaType="Success",
 *                      example="false",
 *                  ),
 *                 @OA\MediaType(
 *                      mediaType="Data",
 *                      example={
 *                      }
 *                  ),
 *                 
 *             @OA\MediaType(
 *                 mediaType="Errors",
 *                 @OA\Schema(
 *                      title="errors",
 *                      @OA\Property(
 *                         property="code_message",
 *                         type="message identifier code",
 *                  ),
 *                  @OA\Property(
 *                         property="field",
 *                         type="field presenting anomaly",
 *                  ),
 *                  @OA\Property(
 *                         property="message",
 *                         type="description of what's going on with the field",
 *                  ),
 *                  example={
 *                              {
 *                                  "code_message": "ERR_ID",
 *                                  "field": "saucer_id",
 *                                  "message": "saucer_id does not exist."
 *                              },
 *                     }
 *                 ),
 *             ),
 *               @OA\MediaType(
 *                 mediaType="Messages",
 *                 @OA\Schema(
 *                      title="errors",
 *                      @OA\Property(
 *                         property="code_message",
 *                         type="message identifier code",
 *                  ),
 *                  @OA\Property(
 *                         property="message",
 *                         type="description of message",
 *                  ),
 *                  example={
 *                              {
 *                                  "code_message": "[ERR_CHECK_DATA]",
 *                                  "message": "The form has errors whit the inputs"
 *                              },
 *                     }
 *                 ),
 *             )
 *         }
 *     )
 * )
 */