<?php
/**
 *  @OA\Post(
 *     path="/api/v1/saucer/register",
 *     tags={"Saucer"},
 *     summary="Register a new saucer",
 *     description="Allows you to register a saucer",
 *     operationId="regSaucer",
 *
 *     @OA\Parameter(
 *         name="Accept",
 *         in="header",
 *         description="application/json",
 *         required=true,
 *         explode=true,
 *     ),
 *     @OA\Parameter(
 *         name="Content-Type",
 *         in="header",
 *         description="application/x-www-form-urlencoded",
 *         required=true,
 *         explode=true,
 *     ),
 *     @OA\Parameter(
 *         name="Authorization",
 *         in="header",
 *         description="Bearer [token]",
 *         required=true,
 *         explode=true,
 *     ),
 *     security={
 *       {"passport": {}},
 *     },
 *     @OA\Parameter(
 *         name="name",
 *         in="query",
 *         description="saucer name",
 *         required=true,
 *         explode=true,
 *    ),
 *     @OA\Parameter(
 *         name="description",
 *         in="query",
 *         description="saucer description",
 *         required=true,
 *         explode=true,
 *    ),
 *    @OA\Parameter(
 *         name="restaurant_id",
 *         in="query",
 *         description="restaurant_id",
 *         required=true,
 *         explode=true,
 *    ),
 *   @OA\Parameter(
 *         name="saucer_category_id",
 *         in="query",
 *         description="id of the category to which the saucer belongs",
 *         required=true,
 *         explode=true,
 *    ),
 *   @OA\Parameter(
 *         name="image",
 *         in="query",
 *         description="url image",
 *         required=true,
 *         explode=true,
 *    ),
 *  @OA\Parameter(
 *         name="products",
 *         in="query",
 *         description="indicates the products can be added to the saucer.Array whit the followings key:
 *                      product_id: product_id,
 *                      default: 1(true) or 0(false). indicates if the product belongs to the initial configuration of the saucer",
 *         required=true,
 *         explode=true,
 *    ),
 *    @OA\Response(
 *         response=200,
 *         description="saucer registered",
 *         content={
 *             @OA\MediaType(
 *                  mediaType="Success",
 *                  example="true",
 *               ),
 *                 @OA\MediaType(
 *                      mediaType="Data",
 *                      example="TODO"
 *                      
 *                  ),
 *               @OA\MediaType(
 *                      mediaType="Errors",
 *                      example={}
 *                      
 *               ),
 *               @OA\MediaType(
 *                 mediaType="Messages",
 *                 @OA\Schema(
 *                      @OA\Property(
 *                         property="code_message",
 *                         type="message identifier code",
 *                  ),
 *                  @OA\Property(
 *                         property="message",
 *                         type="description of message",
 *                  ),
 *                  example={
 *                              {
 *                                  "code_message": "[REGISTERED]",
 *                                  "message": "resource registered"
 *                              },
 *                     }
 *                 ),
 *             )
 *          }
 *     ),
 *     @OA\Response(
 *         response=400,
 *         description="Exist conflict whit the request, please check the errors and messages",
 *         content={
 *                  @OA\MediaType(
 *                      mediaType="Success",
 *                      example="false",
 *                  ),
 *                 @OA\MediaType(
 *                      mediaType="Data",
 *                      example={
 *                      }
 *                  ),
 *                 
 *             @OA\MediaType(
 *                 mediaType="Errors",
 *                 @OA\Schema(
 *                      title="errors",
 *                      @OA\Property(
 *                         property="code_message",
 *                         type="message identifier code",
 *                  ),
 *                  @OA\Property(
 *                         property="field",
 *                         type="field presenting anomaly",
 *                  ),
 *                  @OA\Property(
 *                         property="message",
 *                         type="description of what's going on with the field",
 *                  ),
 *                  example={
 *                              {
 *                                  "code_message": "ERR_REQUIRED",
 *                                  "field": "name",
 *                                  "message": "The name field is required."
 *                              },
 *                              {
 *                                  "code_message": "ERR_EXISTS",
 *                                  "field": "saurce_category_id",
 *                                  "message": "The selected saurce category id is invalid."
 *                              },
 *                              {
 *                                  "code_message": "ERR_ARRAY",
 *                                  "field": "images",
 *                                  "message": "The images must be an array."
 *                              },
 *                     }
 *                 ),
 *             ),
 *               @OA\MediaType(
 *                 mediaType="Messages",
 *                 @OA\Schema(
 *                      title="errors",
 *                      @OA\Property(
 *                         property="code_message",
 *                         type="message identifier code",
 *                  ),
 *                  @OA\Property(
 *                         property="message",
 *                         type="description of message",
 *                  ),
 *                  example={
 *                              {
 *                                  "code_message": "[ERR_CHECK_DATA]",
 *                                  "message": "The form has errors whit the inputs"
 *                              },
 *                     }
 *                 ),
 *             )
 *         }
 *     )
 * )
 * 
 *  @OA\Get(
 *     path="/api/v1/saucer/list/{restaurant_id}",
 *     tags={"Saucer"},
 *     summary="List all the saucers in a restaurant.",
 *     description="Allows you to list  saucers from a restaurant.",
 *     operationId="listSaucers",
 *
 *     @OA\Parameter(
 *         name="Accept",
 *         in="header",
 *         description="application/json",
 *         required=true,
 *         explode=true,
 *     ),
 *     @OA\Parameter(
 *         name="Content-Type",
 *         in="header",
 *         description="application/x-www-form-urlencoded",
 *         required=true,
 *         explode=true,
 *     ), 
 *   @OA\Parameter(
 *         name="restaurnat_id",
 *         in="path",
 *         description="restaurant id",
 *         required=true,
 *         explode=true,
 *    ),
 *    @OA\Response(
 *         response=200,
 *         description="resource was found",
 *         content={
 *             @OA\MediaType(
 *                  mediaType="Success",
 *                  example="true",
 *               ),
 *                 @OA\MediaType(
 *                      mediaType="Data",
 *                      example="TODO"
 *                      
 *                  ),
 *               @OA\MediaType(
 *                      mediaType="Errors",
 *                      example={}
 *                      
 *               ),
 *               @OA\MediaType(
 *                 mediaType="Messages",
 *                 @OA\Schema(
 *                      @OA\Property(
 *                         property="code_message",
 *                         type="message identifier code",
 *                  ),
 *                  @OA\Property(
 *                         property="message",
 *                         type="description of message",
 *                  ),
 *                  example={
 *                              {
 *                                  "code_message": "FOUND",
 *                                  "message": "resource found"
 *                              },
 *                     }
 *                 ),
 *             )
 *          }
 *     ),
 * )
 * 
 * @OA\Get(
 *     path="/api/v1/saucer/show/{restaurant_id}/{saucer_id}",
 *     tags={"Saucer"},
 *     summary="Get saucer with id.",
 *     description="Allows you to get saucer from a restaurant.",
 *     operationId="getSaucerId",
 *
 *     @OA\Parameter(
 *         name="Accept",
 *         in="header",
 *         description="application/json",
 *         required=true,
 *         explode=true,
 *     ),
 *     @OA\Parameter(
 *         name="Content-Type",
 *         in="header",
 *         description="application/x-www-form-urlencoded",
 *         required=true,
 *         explode=true,
 *     ), 
 *     @OA\Parameter(
 *         name="restaurant_id",
 *         in="path",
 *         description="restaurant id",
 *         required=true,
 *         explode=true,
 *    ),
 *     @OA\Parameter(
 *         name="saucer_id",
 *         in="path",
 *         description="id of the saucer",
 *         required=true,
 *         explode=true,
 *    ),
 *    @OA\Response(
 *         response=200,
 *         description="resource was found",
 *         content={
 *             @OA\MediaType(
 *                  mediaType="Success",
 *                  example="true",
 *               ),
 *                 @OA\MediaType(
 *                      mediaType="Data",
 *                      example="TODO"
 *                      
 *                  ),
 *               @OA\MediaType(
 *                      mediaType="Errors",
 *                      example={}
 *                      
 *               ),
 *               @OA\MediaType(
 *                 mediaType="Messages",
 *                 @OA\Schema(
 *                      @OA\Property(
 *                         property="code_message",
 *                         type="message identifier code",
 *                  ),
 *                  @OA\Property(
 *                         property="message",
 *                         type="description of message",
 *                  ),
 *                  example={
 *                              {
 *                                  "code_message": "FOUND",
 *                                  "message": "resource was found"
 *                              },
 *                     }
 *                 ),
 *             )
 *          }
 *     ),
 * )
 * 
 * @OA\Get(
 *     path="/api/v1/saucer/show_by_name/{saurce_name}",
 *     tags={"Saucer"},
 *     summary="Get saucer with name.",
 *     description="Allows you to get saucer from a restaurant.",
 *     operationId="getSaucerName",
 *
 *     @OA\Parameter(
 *         name="Accept",
 *         in="header",
 *         description="application/json",
 *         required=true,
 *         explode=true,
 *     ),
 *     @OA\Parameter(
 *         name="Content-Type",
 *         in="header",
 *         description="application/x-www-form-urlencoded",
 *         required=true,
 *         explode=true,
 *     ), 
 *     @OA\Parameter(
 *         name="saucer_name",
 *         in="path",
 *         description="saucer name",
 *         required=true,
 *         explode=true,
 *    ),
 *    @OA\Response(
 *         response=200,
 *         description="Saucers retrieved successfully",
 *         content={
 *             @OA\MediaType(
 *                  mediaType="Success",
 *                  example="true",
 *               ),
 *                 @OA\MediaType(
 *                      mediaType="Data",
 *                      example="TODO"
 *                      
 *                  ),
 *               @OA\MediaType(
 *                      mediaType="Errors",
 *                      example={}
 *                      
 *               ),
 *               @OA\MediaType(
 *                 mediaType="Messages",
 *                 @OA\Schema(
 *                      @OA\Property(
 *                         property="code_message",
 *                         type="message identifier code",
 *                  ),
 *                  @OA\Property(
 *                         property="message",
 *                         type="description of message",
 *                  ),
 *                  example={
 *                              {
 *                                  "code_message": "[SUCCESS]",
 *                                  "message": "Saucers found"
 *                              },
 *                     }
 *                 ),
 *             )
 *          }
 *     ),
 * )
 * 
 * @OA\Put(
 *     path="/api/v1/saucer/update/{saucer_id}",
 *     tags={"Saucer"},
 *     summary="Update saucer with id.",
 *     description="Allows you to update saucer from a restaurant.",
 *     operationId="updateSaucer",
 *
*     @OA\Parameter(
 *         name="Accept",
 *         in="header",
 *         description="application/json",
 *         required=true,
 *         explode=true,
 *     ),
 *     @OA\Parameter(
 *         name="Content-Type",
 *         in="header",
 *         description="application/x-www-form-urlencoded",
 *         required=true,
 *         explode=true,
 *     ),
 *     @OA\Parameter(
 *         name="Authorization",
 *         in="header",
 *         description="Bearer [token]",
 *         required=true,
 *         explode=true,
 *     ),
 *     security={
 *       {"passport": {}},
 *     },
 *     @OA\Parameter(
 *         name="saucer_id",
 *         in="query",
 *         description="id of the saucer",
 *         required=true,
 *         explode=true,
 *     ),
 *     @OA\Parameter(
 *         name="restaurant_id",
 *         in="query",
 *         description="restaurant id",
 *         required=true,
 *         explode=true,
 *     ),
 *     @OA\Parameter(
 *         name="name",
 *         in="query",
 *         description="saucer name",
 *         required=false,
 *         explode=true,
 *    ),
 *     @OA\Parameter(
 *         name="description",
 *         in="query",
 *         description="saucer description",
 *         required=false,
 *         explode=true,
 *    ),
 *   @OA\Parameter(
 *         name="saucer_category_id",
 *         in="query",
 *         description="id of the category to which the saucer belongs",
 *         required=false,
 *         explode=true,
 *    ),
 *   @OA\Parameter(
 *         name="image",
 *         in="query",
 *         description="url image",
 *         required=false,
 *         explode=true,
 *    ),
 *    @OA\Response(
 *         response=200,
 *         description="saucer updated successfully",
 *         content={
 *             @OA\MediaType(
 *                  mediaType="Success",
 *                  example="true",
 *               ),
 *                 @OA\MediaType(
 *                      mediaType="Data",
 *                      example="TODO"
 *                      
 *                  ),
 *               @OA\MediaType(
 *                      mediaType="Errors",
 *                      example={}
 *                      
 *               ),
 *               @OA\MediaType(
 *                 mediaType="Messages",
 *                 @OA\Schema(
 *                      @OA\Property(
 *                         property="code_message",
 *                         type="message identifier code",
 *                  ),
 *                  @OA\Property(
 *                         property="message",
 *                         type="description of message",
 *                  ),
 *                  example={
 *                              {
 *                                  "code_message": "[UPDATED]",
 *                                  "message": "saucer updated"
 *                              },
 *                     }
 *                 ),
 *             )
 *          }
 *     ),
 *     @OA\Response(
 *         response=400,
 *         description="Exist conflict whit the request, please check the errors and messages",
 *         content={
 *                  @OA\MediaType(
 *                      mediaType="Success",
 *                      example="false",
 *                  ),
 *                 @OA\MediaType(
 *                      mediaType="Data",
 *                      example={
 *                      }
 *                  ),
 *                 
 *             @OA\MediaType(
 *                 mediaType="Errors",
 *                 @OA\Schema(
 *                      title="errors",
 *                      @OA\Property(
 *                         property="code_message",
 *                         type="message identifier code",
 *                  ),
 *                  @OA\Property(
 *                         property="field",
 *                         type="field presenting anomaly",
 *                  ),
 *                  @OA\Property(
 *                         property="message",
 *                         type="description of what's going on with the field",
 *                  ),
 *                  example={
 *                              {
 *                                  "code_message": "ERR_ID",
 *                                  "field": "saucer_id",
 *                                  "message": "saucer_id does not exist."
 *                              },
 *                              {
 *                                  "code_message": "ERR_REQUIRED",
 *                                  "field": "name",
 *                                  "message": "The name field is required."
 *                              },
 *                              {
 *                                  "code_message": "ERR_EXISTS",
 *                                  "field": "saurce_category_id",
 *                                  "message": "The selected saurce category id is invalid."
 *                              },
 *                              {
 *                                  "code_message": "ERR_ARRAY",
 *                                  "field": "images",
 *                                  "message": "The images must be an array."
 *                              },
 *                     }
 *                 ),
 *             ),
 *               @OA\MediaType(
 *                 mediaType="Messages",
 *                 @OA\Schema(
 *                      title="errors",
 *                      @OA\Property(
 *                         property="code_message",
 *                         type="message identifier code",
 *                  ),
 *                  @OA\Property(
 *                         property="message",
 *                         type="description of message",
 *                  ),
 *                  example={
 *                              {
 *                                  "code_message": "[ERR_CHECK_DATA]",
 *                                  "message": "The form has errors whit the inputs"
 *                              },
 *                     }
 *                 ),
 *             )
 *         }
 *     )
 * )
 * @OA\Delete(
 *     path="/api/v1/saucer/delete/{restaurant_id}/{saucer_id}",
 *     tags={"Saucer"},
 *     summary="Delete saucer with id.",
 *     description="Allows you to delete saucer from a restaurant.",
 *     operationId="deleteSaucer",
 *
 *     @OA\Parameter(
 *         name="Accept",
 *         in="header",
 *         description="application/json",
 *         required=true,
 *         explode=true,
 *     ),
 *     @OA\Parameter(
 *         name="Content-Type",
 *         in="header",
 *         description="application/x-www-form-urlencoded",
 *         required=true,
 *         explode=true,
 *     ), 
 *     @OA\Parameter(
 *         name="Authorization",
 *         in="header",
 *         description="Bearer [token]",
 *         required=true,
 *         explode=true,
 *     ),
 *     security={
 *       {"passport": {}},
 *     },
 *     @OA\Parameter(
 *         name="restaurant_id",
 *         in="path",
 *         description="restaurant id",
 *         required=true,
 *         explode=true,
 *    ),
 *     @OA\Parameter(
 *         name="saucer_id",
 *         in="path",
 *         description="id of the saucer",
 *         required=true,
 *         explode=true,
 *    ),
 *    @OA\Response(
 *         response=200,
 *         description="Saucer deleted successfully",
 *         content={
 *             @OA\MediaType(
 *                  mediaType="Success",
 *                  example="true",
 *               ),
 *                 @OA\MediaType(
 *                      mediaType="Data",
 *                      example="TODO"
 *                      
 *                  ),
 *               @OA\MediaType(
 *                      mediaType="Errors",
 *                      example={}
 *                      
 *               ),
 *               @OA\MediaType(
 *                 mediaType="Messages",
 *                 @OA\Schema(
 *                      @OA\Property(
 *                         property="code_message",
 *                         type="message identifier code",
 *                  ),
 *                  @OA\Property(
 *                         property="message",
 *                         type="description of message",
 *                  ),
 *                  example={
 *                              {
 *                                  "code_message": "[DELETED]",
 *                                  "message": "saucer deleted"
 *                              },
 *                     }
 *                 ),
 *             )
 *          }
 *     ),
 *     @OA\Response(
 *         response=400,
 *         description="Exist conflict whit the request, please check the errors and messages",
 *         content={
 *                  @OA\MediaType(
 *                      mediaType="Success",
 *                      example="false",
 *                  ),
 *                 @OA\MediaType(
 *                      mediaType="Data",
 *                      example={
 *                      }
 *                  ),
 *                 
 *             @OA\MediaType(
 *                 mediaType="Errors",
 *                 @OA\Schema(
 *                      title="errors",
 *                      @OA\Property(
 *                         property="code_message",
 *                         type="message identifier code",
 *                  ),
 *                  @OA\Property(
 *                         property="field",
 *                         type="field presenting anomaly",
 *                  ),
 *                  @OA\Property(
 *                         property="message",
 *                         type="description of what's going on with the field",
 *                  ),
 *                  example={
 *                              {
 *                                  "code_message": "ERR_ID",
 *                                  "field": "saucer_id",
 *                                  "message": "saucer_id does not exist."
 *                              },
 *                     }
 *                 ),
 *             ),
 *               @OA\MediaType(
 *                 mediaType="Messages",
 *                 @OA\Schema(
 *                      title="errors",
 *                      @OA\Property(
 *                         property="code_message",
 *                         type="message identifier code",
 *                  ),
 *                  @OA\Property(
 *                         property="message",
 *                         type="description of message",
 *                  ),
 *                  example={
 *                              {
 *                                  "code_message": "[ERR_CHECK_DATA]",
 *                                  "message": "The form has errors whit the inputs"
 *                              },
 *                     }
 *                 ),
 *             )
 *         }
 *     )
 * )
 
 */
