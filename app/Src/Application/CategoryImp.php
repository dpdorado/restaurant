<?php

namespace App\Src\Application;

use App\Http\Resources\Api\BackOffice\Index\CategoryResource;
use App\Interfaces\Application\ICategory;
use App\Models\Category;
use App\Models\Restaurant;
use App\Result\Result;

class CategoryImp implements ICategory
{
    public function __construct()
    {
    }

    /*
    * Permite listar las categorias resgistrados en un restaurante
    */

    public function getAllCategories($restaurant_id): Result
    {
        $result = new Result();

        $categories = Category::where('restaurant_id',$restaurant_id)->get();

        $result->addData('categories', CategoryResource::collection($categories));

        return $result;
    }
}
