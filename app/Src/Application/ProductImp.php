<?php

namespace App\Src\Application;

use App\Http\Resources\Api\BackOffice\Index\ProductResource;
use App\Interfaces\Domain\IProduct as DomainIproduct;
use App\Interfaces\Application\IProduct;
use App\Models\Product;
use App\Result\Result;
use App\Src\Infrastructure\Validators\RegisterProductValidator;
use App\Src\Infrastructure\Validators\UpdateProductValidator;

class ProductImp implements IProduct
{
    protected $productManager;

    public function __construct(DomainIProduct $productManager)
    {
        $this->productManager = $productManager;
    }

    /*
    * Permite hacer el registro de un producto
    */
    public function registerProduct(array $data): Result
    {
        $result = new Result();

        new RegisterProductValidator($data);

        $product = Product::create($data);
        //$this->productManager->storeImageProduct($product->id, $data['images']);
        $result->addData('product', new ProductResource($product));

        return $result;
    }

    /*
    * Permite listar los productos resgistrados en un restaurante
    */
    public function getAllProducts($restaurant_id): Result
    {
        $result = new Result();

        $products = Product::where('restaurant_id', $restaurant_id)->get();
        $result->addData('products', ProductResource::collection($products));

        return $result;
    }

    /*
    * Permite encontrar un producto en un restaurante.
    */
    public function findProduct($restaurant_id, $product_id): Result
    {
        $result = new Result();

        //Error si no esta algún id
        //Como se trata de la misma base de datos no es necesario especificar el id de un restaurante
        //Cuando se use tenancy sera necesario
        /* $product = Product::where('restaurant_id',$restaurant_id)->where('id',$product_id)->first();         */
        $product = Product::findOrFail($product_id);
        $result->addData('product', new ProductResource($product));
        //$result->addData('saucer', new SearchSaucerResource($saucer));//No lo reconoce

        return $result;
    }

    /*
    * Permite obtner un plato por el nombre
    */
    public function getProductByName($restaurant_id, $product_name): Result
    {
        $result = new Result();

        $products = Product::where('restaurant_id', $restaurant_id)->where('name', 'LIKE', "%$product_name%")->get();
        $result->addData('products', ProductResource::collection($products));

        return $result;
    }

    /*
    * Permite actualizar la información de un plato
     */
    public function updateProduct(array $data): Result
    {
        $result = new Result();

        new UpdateProductValidator($data);

        $product = Product::find($data['product_id']);

        /* //Esto deberia ser movido a una ruta, para actualizar imagen por imagen, evitando que el front tenga que enviar todas las imagenes para actualizar solo una
        if (array_key_exists('images', $data)) {
            $this->productManager->deleteImagesProduct($data['product_id']);
            $this->productManager->storeImageProduct($data['product_id'], $data['images']);
        } */
        $product->fill($data);
        $product->save();
        $result->addData('product', new ProductResource($product));

        return $result;
    }

    /*
    * Permite eliminar un producto de un restaurante
    */
    public function deleteProduct($restaurant_id, $product_id): Result
    {
        $result = new Result();
        
        //$restaurant = Restaurant::findOrFail($restaurant_id);
        //comovalido que saurce exista?? //Error si no esta algún id
        $product = Product::findOrFail($product_id);
        
        //$saucer = Saucer::findorFail($saucer_id);       
        //$this->productManager->deleteImagesProduct($product_id);
        $product->delete();
        $result->addData('product', new ProductResource($product));

        return $result;
    }
}
