<?php

namespace App\Src\Application;

use App\Interfaces\Domain\IShoppingCart as DomainIShoppingCart;
use App\Http\Resources\Api\Shared\Search\ShoppingCartResource;
use App\Http\Resources\Api\Shared\Search\SaleItemResource;
use App\Interfaces\Application\IShoppingCart;
use App\Models\billofsale;
use App\Models\ShoppingCart;
use App\Models\SaleItem;
use App\Result\Result;
use Illuminate\Support\Facades\DB;
use App\Src\Infrastructure\Validators\AddItemCartValidator;
use Billofsale as GlobalBillofsale;

class ShoppingCartImpl implements IShoppingCart
{
    protected $ShoppingCartManager;

    public function __construct(DomainIShoppingCart $ShoppingCartManager)
    {
        $this->ShoppingCartManager = $ShoppingCartManager;
    }

    public function addItem(array $data): Result
    {
        new AddItemCartValidator($data);

        return $this->ShoppingCartManager->addItem($data);
    }

    public function deleteItem($id): Result
    {
        $result = new Result();
        $item=SaleItem::findOrFail($id);
        $item->delete();
        $result->addData("item_sale", new SaleItemResource($item));
        //todo el elimnar lanza un exception

        return $result;
    }
    public function BillOfSale(array $data): Result
    {

        DB::insert('insert into billofsales (name,email,addres,phone,pay,created_at,updated_at) values (?,?,?,?,?,?,?)',[$data['name'],$data['email'],$data['address'],$data['phone'],$data['pay'],'2020-09-08 13:20:07','2020-09-04 13:20:07']);
        $orden = $data['shopping_cart_id'] ;
        $result = new Result();
        $item=SaleItem::where('shopping_cart_id',$data['shopping_cart_id'])->get();
        foreach($item as $items){
            $items->order_id = $orden;
            $items->save();
        }
        return $result;

    }
    public function BillOfSaleList():object{
        $result = new Result();

        $result = DB::table('billofsales')->get();

        return $result;
    }

    public function deleteBillOfSale($id):Result{
        $result = new Result();

        //$item=billofsale::findOrFail($id);

        $item = DB::table('billofsales')->where('id',$id);
        $item->delete();
        $result->addData("item_sale", new SaleItemResource($item));

        return $result;
    }
    public function showShoppingCart($shopping_cart_id): Result
    {
        $result = new Result();
        $shoppinCart = ShoppingCart::findOrFail($shopping_cart_id);
        $result->addData('shopping_cart', new ShoppingCartResource($shoppinCart));

        return $result;
    }

    public function RegisterOrder($total_cost,$state,$user_id){
        DB::insert('insert into order (total_cost, state ,user_id  values (?, ?, ?)', [$total_cost, 'confirm',1]);

    }
    }

