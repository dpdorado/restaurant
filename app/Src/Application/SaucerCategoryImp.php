<?php

namespace App\Src\Application;

use App\Src\Infrastructure\Validators\RegisterSaucerCategoryValidator;
use App\Http\Resources\Api\BackOffice\Index\SaucerCategoryResource;
use App\Interfaces\Application\ISaucerCategory;
use App\Models\SaucerCategory;
use App\Models\Saucer;
use App\Models\Restaurant;
use App\Result\Result;
use DB;

class SaucerCategoryImp implements ISaucerCategory
{
    public function __construct()
    {
    }

    /*
        Permite registrar una categoria de platillos
        @param :
            data: Informacon registrar por el usuario para una categoria
        @return:
            repuesta json de proceso exitoso
        @exeption
            (create)Genera exeption si tabla no existe en la base de datos
    */

    public function registerCategory(array $data): Result
    {
        $result = new Result();

        new RegisterSaucerCategoryValidator($data);

        $saucer = SaucerCategory::create($data);

        $result->addData('category', new SaucerCategoryResource($saucer));

        return $result;
    }

     /*
     *   Permite listar todas las categorias
        @param :
            vacio
        @return:
            json con todas la categorias
        @exeption
            (NotFoundExcpetion)Genera exeption si no existe categorias en la base de datos
    */
    public function getAllCategories(): Result
    {
        $result = new Result();

        $categories= SaucerCategory::all();

        if(!$categories){
            throw new NotFoundException();
        }

        $result->addData('categories', SaucerCategoryResource::collection($categories));

        return $result;
    }
    /*
    *   Permite listar las categorias por restaurante
        @param :
            restaurant_id: corresponde al id del restaurante
        @return:
            json con categorias correspondientes a restaurante
        @exeption
            (NotFoundException)Genera exeption si no existe categorias en la base de datos
    */

    public function getCategoriesByRestaurant($restaurant_id): Result
    {
        $result = new Result();

        //TODO: Actualizar cansulta, no esta retornandoo lo esperado

        $categories= SaucerCategory::select('saucer_category.id','saucer_category.name')
        ->join('saucers', 'saucers.saucer_category_id', '=', 'saucer_category.id')
        ->where('saucers.restaurant_id',$restaurant_id)
        ->join('restaurants', 'restaurants.id', '=', 'saucers.restaurant_id')
        ->get();

        if(!$categories){
            throw new NotFoundException();
        }

        $result->addData('categories', SaucerCategoryResource::collection($categories));

        return $result;
    }

    /*
    *   Permite encontar una categoria de platillo
        @param :
            saucer_category_id: corresponde a la categoria de platillo
        @return:
            json con categorias correspondientes a categoria platillo
        @exeption
            (finOrdFail)Genera exeption si no existe saucer_category_id en la base de datos
    */
    public function findCategory($saucer_category_id): Result
    {
        $result = new Result();

        $category = SaucerCategory::findOrFail($saucer_category_id);

        $result->addData('category', new SaucerCategoryResource($category));

        return $result;
    }

    /*
    * Permite obtner listar las categorias con un nombre
        @param :
            saucer_category_name:
        @return:
            json con lista de categorias
        @exeption
            vacio
    */
    public function getCategoryByName($saucer_category_name): Result
    {
        $result = new Result();

        $categories = SaucerCategory::where('name', 'LIKE', "%$saucer_category_name%")->get();

        $result->addData('categories', SaucerCategoryResource::collection($categories));

        return $result;
    }

    /*
    * Permite actualizar la información de una categoria de platill0
     */
    public function updateCategory(array $data): Result
    {
        $result = new Result();

        new RegisterSaucerCategoryValidator($data);

        //Arreglar
        $category = SaucerCategory::findOrFail($data['id']);
        $category->name = $data['name'];
        $category->save();

        $result->addData('category', new SaucerCategoryResource($category));

        return $result;
    }

    /*
    * Permite una categoria de platillo
    */
    public function deleteCategory($saucer_category_id): Result
    {
        $result = new Result();

        $category = SaucerCategory::findorFail($saucer_category_id);
        $category->delete();

        $result->addData('category', new SaucerCategoryResource($category));

        return $result;
    }
}
