<?php

namespace App\Src\Application;

use App\Http\Resources\Api\BackOffice\Index\RestaurantsResource;
use App\Interfaces\Application\IRestaurant;
use App\Models\Restaurant;
use App\Result\Result;
use App\Interfaces\Domain\IRestaurant as DomainIRestaurant;
use App\Src\Infrastructure\Validators\RegisterRestaurantValidator;

class RestaurantImpl implements IRestaurant
{
    protected $restaurantManager;

    public function __construct(DomainIRestaurant $restaurantManager)
    {
        $this->restaurantManager = $restaurantManager;
    }
     /*
        Permite registrar un restaurante
        @param :
            data: corresponde a la informacion ingresa por el usuario para registrar un restaurante
        @return:
            respuesta json proceso exitoso
        @exeption
            (create)Genera exeption si la tabla no existe en la base de datos
    */
    public function registerRestaurant(array $data): Result
    {
        $result = new Result();

        new RegisterRestaurantValidator($data);//se valida la informacion ingresada por el usuario
        $restaurant = Restaurant::create($data);
        /* $this->restaurantManager->storeImageRestaurant($restaurant->id, $data['images']); */
        $result->addData('restaurant', new RestaurantsResource($restaurant));

        return $result;
    }
    /*
        Permite listar todos los restaurantes
        @param : vacio
        @return: restaurantes
    */

    public function getAllRestaurants(): Result
    {

        $result = new Result();

        $restaurants = Restaurant::all();
        $result->addData('retaurants', RestaurantsResource::collection($restaurants));

        return $result;
    }
     /*
        Permite eliminar un restaurante
        @param :
            restaurant_id: corresponde al id del restaurante que queremos eliminar
        @return:
            vacio
        @exeption
            (findOrFail)Genera exeption si el restaurante no existe en la base de datos
    */
    public function deleteRestaurant($restaurant_id): Result
    {

        $result = new Result();

        $restaurant = Restaurant::findOrFail($restaurant_id);
        /* $this->restaurantManager->deleteImagesRestaurant($restaurant_id); */
        $restaurant->delete();
        $result->addData('restaurants', new RestaurantsResource($restaurant));

        return $result;
    }

    /*
    public function showSaucer($saucer_id): Result //Por que se le llamo getResourceID? pudo llamarce showResource o FindResource
    {
        $result = new Result();

        $saucer = Saucer::findOrFail($saucer_id);
        $result->addData('saucer', new SearchSaucerResource($saucer));

        return $result;
    }

    public function getSaucerByName($saucer_name): Result
    {
        $result = new Result();

        $saucers = Saucer::where('name', 'LIKE', "%$saucer_name%")->get();
        $result->addData('saucers', SaucerResource::collection($saucers));

        return $result;
    }

    public function updateSaucer(array $data): Result
    {
        $result = new Result();

        new UpdateSaucerValidator($data);
        $saucer = Saucer::find($data['saucer_id']);

        //Esto deberia ser movido a una ruta, para actualizar imagen por imagen, evitando que el front tenga que enviar todas las imagenes para actualizar solo una
        if (array_key_exists('images', $data)) {
            $this->saucerManager->deleteImagesSaucer($data['saucer_id']);
            $this->saucerManager->storeImageSaucer($data['saucer_id'], $data['images']);
        }
        $saucer->fill($data);
        $saucer->save();
        $result->addData('saucer', new SearchSaucerResource($saucer));

        return $result;
    }
*/
}
