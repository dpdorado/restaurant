<?php

namespace App\Src\Application;

use App\Exceptions\Api\NotFoundException;
use App\Http\Resources\Api\BackOffice\Index\SaucerResource;
use App\Http\Resources\Api\BackOffice\Search\SaucerResource as SearchSaucerResource;
use App\Interfaces\Domain\ISaucer as DomainISaucer;
use App\Interfaces\Application\ISaucer;
use App\Models\Saucer;
use App\Result\Result;
use App\Src\Infrastructure\Validators\RegisterSaucerValidator;
use App\Src\Infrastructure\Validators\UpdateSaucerValidator;

class SaucerImp implements ISaucer
{
    protected $saucerManager;

    public function __construct(DomainISaucer $saucerManager)
    {
        $this->saucerManager = $saucerManager;
    }

    public function registerSaucer(array $data): Result
    {
        $result = new Result();

        
        new RegisterSaucerValidator($data);        
        
        $saucer = Saucer::create($data);

        $saucer->products()->sync($data['products']);
                
       /*  if (array_key_exists('images',$data)){
            $this->saucerManager->storeImageSaucer($saucer->id, $data['images']);
        }     */    
        
        $result->addData('saucer', new SaucerResource($saucer));

        return $result;
    }

    /*
    * Permite listar los platos resgistrados en un restaurante
    */
    public function getAllSaucers($restaurant_id): Result
    {
        $result = new Result();

        $saucers = Saucer::where('restaurant_id', $restaurant_id)->get();

        $result->addData('saucers', SaucerResource::collection($saucers));

        return $result;
    }

    /*
    * Permite encontrar un plato en un restaurante.
    */
    public function findSaucer($restaurant_id, $saucer_id): Result
    {
        $result = new Result();

        //Error si no esta algún id
        $saucer = Saucer::where('restaurant_id', $restaurant_id)->where('id', $saucer_id)->first();
        if(!$saucer){
            throw new NotFoundException();
        }
        $result->addData('saucer', new SaucerResource($saucer));
        //$result->addData('saucer', new SearchSaucerResource($saucer));//No lo reconoce

        return $result;
    }

    /*
    * Permite obtner un plato por el nombre
    */
    public function getSaucerByName($restaurant_id, $saucer_name): Result
    {
        $result = new Result();

        $saucers = Saucer::where('restaurant_id', $restaurant_id)->where('name', 'LIKE', "%$saucer_name%")->get();

        $result->addData('saucers', SaucerResource::collection($saucers));

        return $result;
    }

    /*
    * Permite actualizar la información de un plato
     */
    public function updateSaucer(array $data): Result
    {
        $result = new Result();

        new UpdateSaucerValidator($data);

        $saucer = Saucer::where('restaurant_id', $data['restaurant_id'])->where('id', $data['saucer_id'])->first();

        /* //Esto deberia ser movido a una ruta, para actualizar imagen por imagen, evitando que el front tenga que enviar todas las imagenes para actualizar solo una
        if (array_key_exists('images', $data)) {
            $this->saucerManager->deleteImagesSaucer($data['saucer_id']);
            $this->saucerManager->storeImageSaucer($data['saucer_id'], $data['images']);
        } */
        $saucer->fill($data);
        $saucer->save();
        $result->addData('saucer', new SaucerResource($saucer));

        return $result;
    }

    /*
    * Permite eliminar un plato de un restaurante
    */
    public function deleteSaucer($restaurant_id, $saucer_id): Result
    {
        $result = new Result();

        //$restaurant = Restaurant::findOrFail($restaurant_id);
        //comovalido que saurce exista?? //Error si no esta algún id
        $saucer = Saucer::where('restaurant_id', $restaurant_id)->where('id', $saucer_id)->first();
        if(!$saucer){
            throw new NotFoundException();
        }
        //$saucer = Saucer::findorFail($saucer_id);       
        /* $this->saucerManager->deleteImagesSaucer($saucer_id); */
        $saucer->delete();
        $result->addData('saucer', new SaucerResource($saucer));

        return $result;
    }
}
