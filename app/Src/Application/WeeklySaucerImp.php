<?php

namespace App\Src\Application;

use App\Exceptions\Api\NotFoundException;
use App\Http\Resources\Api\BackOffice\Index\WeeklySaucerResource;
use App\Interfaces\Application\IWeeklySaucer;
use App\Models\WeeklySaucer;
use App\Result\Result;
use App\Src\Infrastructure\Validators\RegisterWeeklySaucerValidator;

class WeeklySaucerImp implements IWeeklySaucer
{    

    public function __construct(){}

    
    public function registerWeeklySaucer(array $date): Result{
        $result = new Result();

        new RegisterWeeklySaucerValidator($date);        
        
        $weekly_saucer = WeeklySaucer::create($date);          
        
        $result->addData('weekly_saucer', new WeeklySaucerResource($weekly_saucer));
        return $result;

    }    
    public function findWeeklySaucersDate($restaurant_id,$date): Result{
        $result = new Result();
                 
        $WeeklySaucer = WeeklySaucer::where('restaurant_id', $restaurant_id)->where('fecha', $date)->get();
        
        if(!$WeeklySaucer){
            throw new NotFoundException();
        }            
        $result->addData('weekly_saucer', WeeklySaucerResource::collection($WeeklySaucer));

        return $result;
    }    
      
}

