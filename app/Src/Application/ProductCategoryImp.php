<?php

namespace App\Src\Application;

use App\Http\Resources\Api\BackOffice\Index\ProductCategoryResource;
use App\Src\Infrastructure\Validators\RegisterProductCategoryValidator;
use App\Interfaces\Application\IProductCategory;
use App\Models\CategoryProduct;
use App\Models\Restaurant;
use App\Result\Result;

class ProductCategoryImp implements IProductCategory
{
    public function __construct()
    {
    }
    /*
    *   Permite registrar categorias
        @param :
            data: corresponde a la informacion ingresa por el usuario para registrar una categoria
        @return:
            respuesta json proceso exitoso
        @exeption
            (create)Genera exeption si la tabla no existe en la base de datos
    */
    public function registerCategory(array $data): Result
    {
        $result = new Result();

        new RegisterProductCategoryValidator($data);

        $category = CategoryProduct::create($data);

        $result->addData('category', new ProductCategoryResource($category));

        return $result;
    }
    /*
    * Permite listar las categorias
        @param :
            vacio
        @return:
            respuesta json lista de categorias
        @exeption
            (NotFoundException)Genera exeption si la tabla no existe en la base de datos
    */
    public function getAllCategories(): Result
    {
        $result = new Result();

        $categories = CategoryProduct::all();

        if(!$categories){
            throw new NotFoundException();
        }

        $result->addData('categories', ProductCategoryResource::collection($categories));

        return $result;
    }
    /*
    * Devuelve las categorias que tienen productos en un restaurante
    */
    public function getCategoriesByRestaurant($restaurant_id): Result
    {
        $result = new Result();

        //TODO: Actualizar cansulta, no esta retornandoo lo esperado

        $categories= CategoryProduct::select('categories_products.*')
        ->join('products', 'products.categories_product_id', '=', 'categories_products.id')
        ->join('restaurants', 'restaurants.id', '=', 'products.restaurant_id')
        ->get();

        if(!$categories){
            throw new NotFoundException();
        }

        $result->addData('categories', ProductCategoryResource::collection($categories));

        return $result;
    }

     /*
    * Permite encontrar una categoria de un producto
    */
    public function findCategory($product_category_id): Result
    {
        $result = new Result();

        $category = CategoryProduct::findOrFail($product_category_id);

        $result->addData('category', new ProductCategoryResource($category));

        return $result;
    }

    /*
    * Permite obtner listar las categorias con un nombre
    */
    public function getCategoryByName($product_category_name): Result
    {
        $result = new Result();

        $categories = CategoryProduct::where('name', 'LIKE', "%$product_category_name%")->get();

        $result->addData('categories', ProductCategoryResource::collection($categories));

        return $result;
    }

    /*
    * Permite actualizar la información de una categoria de producto
     */
    public function updateCategory(array $data): Result
    {
        $result = new Result();

        new RegisterProductCategoryValidator($data);

        //Arreglar
        $category = CategoryProduct::findOrFail($data['id']);
        $category->name = $data['name'];
        $category->save();

        $result->addData('category', new ProductCategoryResource($category));

        return $result;
    }

    /*
    * Permite una categoria de producto
    */
    public function deleteCategory($product_category_id): Result
    {
        $result = new Result();

        $category = CategoryProduct::findorFail($product_category_id);
        $category->delete();

        $result->addData('category', new ProductCategoryResource($category));

        return $result;
    }
}
