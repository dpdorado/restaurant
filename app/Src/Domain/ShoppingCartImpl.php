<?php

namespace App\Src\Domain;

use App\Http\Resources\Api\Shared\Search\SaleItemResource;
use App\Interfaces\Domain\IShoppingCart;
use App\Models\Product;
use App\Models\SaleItem;
use App\Models\Saucer;
use App\Models\User;
use App\Result\Result;

class ShoppingCartImpl implements IShoppingCart
{
    public function addItem(array $data): Result
    {
        $result = new Result();

        //Importante!!
        //Aqui solo se esta tomando el usuario con id 1
        //Luego cuando este la parte de autentificacion tomar el que este autentificado

        //****** */
        $user = User::findOrFail(1);
        //***** */
        $cart = $user->cart;

        $data['shopping_cart_id'] = $cart->id;

        //Importante!!
        //Agregar transaccion para restar de stock y agregar a carrito
        //Ademas de validaciones de stock
        $product = Product::find($data['product_id']);
        $data['unit_price'] = $product->cost;
        $item = SaleItem::create($data);

        $result->addData('item', new SaleItemResource($item));

        return $result;
    }
}
