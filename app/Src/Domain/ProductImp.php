<?php

namespace App\Src\Domain;

use App\Interfaces\Domain\IProduct;
use Illuminate\Support\Facades\File;
use App\Models\ProductImage;


class ProductImp implements IProduct
{
    /*
      metodo para almacenar las imagenes en la direccion fisica "files/product", el parametro "product_id" identifica
      el id del producto correspondiente y el parametro "images" la coleccion de imagenes que pertenecen al producto
      y puede genera como respuesta es una exeption.
    */
    public function storeImageProduct($product_id, array $images)
    {

        $base_path = 'files/product/';//donde se encuentran alamcenadas las imagenes
        $i = 0;

        foreach ($images as $img) {
            try {
                $fileName = time() . $i . '.' . $img->extension();//se le concatena la extencion

                $img->move(public_path($base_path), $fileName);//se las imagenes a la direccion base_path que es la carpeta publica/files/product
                ProductImage::create([
                    'path' => $base_path . $fileName,//direccion de almacenamiento de la imagen
                    'product_id' => $product_id,
                ]);
                $i++;//contados para recorres todo el array de imagenes
            } catch (\App\Exceptions\Api\FileException $th) {

                throw $th;
            }
        }
    }
    /*
   Metodo para eliminar las imagenes de un producto y resive como parametro el id
    del producto y respuesta puedecer ser una exeption ne caso de que la imagne no exista
    */
    public function deleteImagesProduct($product_id)
    {
        $images = ProductImage::where('product_id', $product_id)->get();

        foreach ($images as $img) {
            $mi_imagen = public_path() . '/' . $img->path;
            if (File::exists($mi_imagen)) {
                File::delete($mi_imagen);
            }
            $img->delete();
        }
    }
}
