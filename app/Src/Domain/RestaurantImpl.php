<?php

namespace App\Src\Domain;

use App\Interfaces\Domain\IRestaurant;
use App\Models\RestaurantImage;
use Illuminate\Support\Facades\File;

/*
      metodo para almacenar las imagenes en la direccion fisica "files/restaurant",  restaurant_id identifica
      que restaurant le corresponde las imagenes. y pude genera como respuesta un exeption.
 */
class RestaurantImpl implements IRestaurant
{
    public function storeImageRestaurant($restaurant_id, array $images)
    {
        $base_path = 'files/restaurant/';

        $i = 0;

        foreach ($images as $img) {
            try {
                $fileName = time() . $i . '.' . $img->extension();

                $img->move(public_path($base_path), $fileName);
                RestaurantImage::create([
                    'path' => $base_path . $fileName,
                    'restaurant_id' => $restaurant_id,
                ]);
                $i++;
            } catch (\App\Exceptions\Api\FileException $th) {

                throw $th;
            }
        }
    }
    /*
        Metodo para eliminar las imagenes de un restaurante y resive como parametro el id
        del restaurante
    */
    public function deleteImagesRestaurant($restaurant_id)
    {

        $images = RestaurantImage::where('restaurant_id', $restaurant_id)->get();

        foreach ($images as $img) {
            $mi_imagen = public_path() . '/' . $img->path;
            if (File::exists($mi_imagen)) {
                File::delete($mi_imagen);
            }
            $img->delete();
        }
    }
}
