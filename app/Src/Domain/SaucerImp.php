<?php

namespace App\Src\Domain;

use App\Interfaces\Domain\ISaucer;
use Illuminate\Support\Facades\File;
use App\Models\SaucerImage;


class SaucerImp implements ISaucer
{

    /*
      metodo para almacenar las imagenes en la direccion fisica "files/saucer", el parametro "saucer_id" identifica
      el id del Plato correspondiente y el parametro "images" la coleccion de imagenes que pertenecen al Plato(Saucer)
    */

    public function storeImageSaucer($saucer_id, array $images)
    {

        $base_path = 'files/saucer/';
        $i = 0;

        foreach ($images as $img) {
            try {
                $fileName = time() . $i . '.' . $img->extension();

                $img->move(public_path($base_path), $fileName);
                SaucerImage::create([
                    'path' => $base_path . $fileName,
                    'saucer_id' => $saucer_id,
                ]);
                $i++;
            } catch (\App\Exceptions\Api\FileException $th) {

                throw $th;
            }
        }
    }

    public function deleteImagesSaucer($saucer_id)
    {
        $images = SaucerImage::where('saucer_id', $saucer_id)->get();

        foreach ($images as $img) {
            $mi_imagen = public_path() . '/' . $img->path;
            if (File::exists($mi_imagen)) {
                File::delete($mi_imagen);
            }
            $img->delete();
        }
    }
}
