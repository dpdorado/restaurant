<?php

namespace App\Src\Infrastructure\Validators;

use Illuminate\Support\Facades\Validator;


class RegisterWeeklySaucerValidator extends BaseValidator
{
    public function __construct(array $data)
    {

        $validator = Validator::make($data, [
            'fecha' => ['required', 'date'],            
            'saucer_id' => ['required', 'integer', 'exists:saucers,id'],
            'restaurant_id' => ['required', 'integer', 'exists:restaurants,id']            
        ]);

        parent::__construct($validator);
    }
}