<?php

namespace App\Src\Infrastructure\Validators;


use Illuminate\Support\Facades\Validator;


class UpdateSaucerValidator extends BaseValidator
{
    public function __construct(array $data)
    {

        $validator = Validator::make($data, [
            'saucer_id' => ['required', 'integer', 'exists:saucers,id'],
            'name' => ['string', 'min:3', 'max:30'],
            'description' => ['string', 'max:250'],
            'saucer_category_id' => ['integer', 'exists:saucer_category,id'],
            'restaurant_id' => ['required','integer', 'exists:restaurants,id'],
            'image' => ['string'],

        ]);

        parent::__construct($validator);
    }
}
