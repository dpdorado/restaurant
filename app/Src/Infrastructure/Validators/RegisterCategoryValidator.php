<?php

namespace App\Src\Infrastructure\Validators;

use Illuminate\Support\Facades\Validator;


class RegisterCategoryValidator extends BaseValidator
{
    public function __construct(array $data)
    {

        $validator = Validator::make($data, [
            'name' => ['required', 'string', 'min:3', 'max:30'],
            'description' => ['required', 'string', 'max:250'],        
            'restaurant_id' => ['required', 'integer', 'exists:restaurants,id']
        ]);

        parent::__construct($validator);
    }
}
