<?php

namespace App\Src\Infrastructure\Validators;

use Illuminate\Support\Facades\Validator;


class RegisterProductValidator extends BaseValidator
{
    public function __construct(array $data)
    {

        $validator = Validator::make($data, [
            'name' => ['required', 'string', 'min:3', 'max:30'],
            'description' => ['required', 'string', 'max:250'],
            'cost' => ['required', 'numeric','min:1','max:120000000'],
            'categories_product_id' => ['required', 'integer', 'exists:categories_products,id'],
            'stock'=>['required', 'integer','min:1'],
            'restaurant_id' => ['required', 'integer', 'exists:restaurants,id'],
            'image' => ['required', 'string'],
        ]);

        parent::__construct($validator);
    }
}