<?php

namespace App\Src\Infrastructure\Validators;

use Illuminate\Support\Facades\Validator;


class RegisterProductCategoryValidator extends BaseValidator
{
    public function __construct(array $data)
    {

        $validator = Validator::make($data, [
            'name' => ['required', 'string', 'min:3', 'max:30']            
        ]);

        parent::__construct($validator);
    }
}