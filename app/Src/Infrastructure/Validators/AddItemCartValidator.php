<?php

namespace App\Src\Infrastructure\Validators;

use Illuminate\Support\Facades\Validator;


class AddItemCartValidator extends BaseValidator
{
    public function __construct(array $data)
    {
        $validator = Validator::make($data, [
            'quantity' => ['required', 'integer', 'min:1'],
            'product_id' => ['required', 'integer', 'exists:products,id'],
        ]);

        parent::__construct($validator);
    }
}
