<?php

namespace App\Src\Infrastructure\Validators;

use Illuminate\Support\Facades\Validator;


class RegisterSaucerValidator extends BaseValidator
{
    public function __construct(array $data)
    {

        $validator = Validator::make($data, [
            'name' => ['required', 'string', 'min:3', 'max:30'],
            'description' => ['required', 'string', 'max:250'],
            'saucer_category_id' => ['required', 'integer', 'exists:saucer_category,id'],
            'restaurant_id' => ['required', 'integer', 'exists:restaurants,id'],
            'image' => ['nullable','string'],

            'products' => ['required', 'array'],
            'products.*.product_id' => ['required', 'integer', 'exists:products,id'],
            'products.*.default' => ['required', 'integer', 'in:1,0']
        ]);

        parent::__construct($validator);
    }
}
