<?php

namespace App\Src\Infrastructure\Validators;

use Illuminate\Support\Facades\Validator;

class LoginValidator extends BaseValidator
{
    public function __construct(array $data)
    {
        $validator = Validator::make($data, [
            'email' => ['required', 'email'],
            'password' => ['required', 'string'],
        ]);

        parent::__construct($validator);
    }
}
