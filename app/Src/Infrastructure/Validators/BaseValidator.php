<?php

namespace App\Src\Infrastructure\Validators;

use App\Exceptions\Api\CheckDataException;
use Illuminate\Contracts\Validation\Validator;

class BaseValidator
{
    protected $validator;

    public function __construct(Validator $validator)
    {
        $this->validator = $validator;
        $this->validate();
    }

    private function validate()
    {

        if ($this->validator->fails()) {

            $exception = new CheckDataException();
            $exception->addErrors($this->validator->errors());

            throw $exception;
        }
    }
}
