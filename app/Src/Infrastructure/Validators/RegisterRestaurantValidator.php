<?php

namespace App\Src\Infrastructure\Validators;

use Illuminate\Support\Facades\Validator;


class RegisterRestaurantValidator extends BaseValidator
{
    public function __construct(array $data)
    {

        $validator = Validator::make($data, [
            'name' => ['required', 'string', 'min:3', 'max:30'],
            'description' => ['required', 'string', 'max:250'],
            'address' => ['required', 'string', 'max:250'],
            'phone' => ['required', 'string', 'max:250'],
            'domain' => ['required', 'string', 'max:300'],
            'image' => ['required','string'], //todo cambiar por string
            
        ]);

        parent::__construct($validator);
    }
}
