<?php

namespace App\Src\Infrastructure\Validators;

use Illuminate\Support\Facades\Validator;


class UpdateProductValidator extends BaseValidator
{
    public function __construct(array $data)
    {

        $validator = Validator::make($data, [
            'product_id' => ['required', 'integer', 'exists:products,id'],
            'name' => ['string', 'min:3', 'max:30'],
            'description' => ['string', 'max:250'],
            'cost' => ['numeric', 'min:1', 'max:120000000'],
            'stock' => ['numeric', 'integer'],
            'category_id' => ['integer', 'exists:categories_products,id'],
            'restaurant_id' => ['integer', 'exists:restaurants,id'],
            'image' => ['string'],
            
        ]);

        parent::__construct($validator);
    }
}