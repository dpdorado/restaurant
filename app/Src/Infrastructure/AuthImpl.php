<?php

namespace App\Src\Infrastructure;

use App\Exceptions\Api\UnauthorizedPassportException;
use App\Src\Infrastructure\Validators\LoginValidator;
use App\Interfaces\Infrastructure\IAuth;
use Illuminate\Support\Facades\Auth;
use App\Result\Result;


class AuthImpl implements IAuth
{
    public function login(array $data): Result
    {
        $result = new Result();
        
        new LoginValidator($data);

        if (!Auth::attempt($data)) {
            throw new UnauthorizedPassportException();
        }

        $user = Auth::user();
        $accessToken = $user->createToken('authToken')->accessToken;

        $result->addData('access_token', $accessToken);
        $result->addData('roles', $user->getRoleNames()->all());

        return $result;
    }

    public function logout(): Result
    {
        $result = new Result();

        $user = Auth::user();
        $user->tokens()->update(['revoked' => true]); //Revokamos TODOS los tokens del usuario
        $user->save();

        return $result;
    }
}
