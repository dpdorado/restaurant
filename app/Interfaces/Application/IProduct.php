<?php

namespace App\Interfaces\Application;

use App\Result\Result;

interface IProduct
{
    public function registerProduct(array $data): Result;
    public function getAllProducts($restaurant_id): Result;
    public function findProduct($restaurant_id, $product_id): Result;
    public function getProductByName($restaurant_id, $product_name): Result;
    public function updateProduct(array $data): Result;
    public function deleteProduct($restaurant_id, $product_id): Result;
}
