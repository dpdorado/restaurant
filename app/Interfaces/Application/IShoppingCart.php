<?php

namespace App\Interfaces\Application;

use App\Result\Result;

interface IShoppingCart
{
    public function addItem(array $data): Result;
    public function deleteItem($id): Result;
    public function showShoppingCart($shopping_cart_id): Result;
    public function BillOfSale(array $data): Result;
    public function BillOfSaleList(): object;
    public function deleteBillOfSale($id): Result;



}
