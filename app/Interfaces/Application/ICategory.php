<?php

namespace App\Interfaces\Application;

use App\Result\Result;

interface ICategory
{    
    public function getAllCategories($restaurant_id): Result; 
}
