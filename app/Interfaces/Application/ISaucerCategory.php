<?php

namespace App\Interfaces\Application;

use App\Result\Result;

interface ISaucerCategory
{    
    public function registerCategory(array $data): Result;
    public function getAllCategories(): Result;         
    public function findCategory($saucer_category_id): Result;
    public function getCategoryByName($saucer_category_name): Result;
    public function updateCategory(array $data): Result;
    public function deleteCategory($saucer_category_id): Result;


    public function getCategoriesByRestaurant($restaurant_id): Result;
}