<?php

namespace App\Interfaces\Application;

use App\Result\Result;

interface IWeeklySaucer
{
    public function registerWeeklySaucer(array $date): Result;    
    public function findWeeklySaucersDate($restaurant_id,$date): Result;    
}