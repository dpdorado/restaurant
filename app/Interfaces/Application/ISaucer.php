<?php

namespace App\Interfaces\Application;

use App\Result\Result;

interface ISaucer
{
    public function registerSaucer(array $data): Result;
    public function getAllSaucers($restaurant_id): Result;
    public function findSaucer($restaurant_id, $saucer_id): Result;
    public function getSaucerByName($restaurant_id, $saucer_name): Result;
    public function updateSaucer(array $data): Result;
    public function deleteSaucer($restaurant_id, $saucer_id): Result;
}
