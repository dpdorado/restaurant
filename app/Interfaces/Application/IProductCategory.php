<?php

namespace App\Interfaces\Application;

use App\Result\Result;

interface IProductCategory
{    

    public function registerCategory(array $data): Result;
    public function getAllCategories(): Result;     

    public function findCategory($product_category_id): Result;
    public function getCategoryByName($product_category_name): Result;
    public function updateCategory(array $data): Result;
    public function deleteCategory($product_category_id): Result;

    public function getCategoriesByRestaurant($restaurant_id): Result;
}