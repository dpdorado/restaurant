<?php

namespace App\Interfaces\Application;

use App\Result\Result;

interface IRestaurant
{

    public function getAllRestaurants(): Result;
    public function registerRestaurant(array $data): Result;
    public function deleteRestaurant($restaurant_id): Result;

}
