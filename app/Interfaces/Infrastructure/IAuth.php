<?php

namespace App\Interfaces\Infrastructure;

use App\Result\Result;


interface IAuth
{

    public function login(array $data): Result;
    public function logout(): Result;
}
