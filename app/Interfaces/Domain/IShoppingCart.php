<?php

namespace App\Interfaces\Domain;

use App\Result\Result;

interface IShoppingCart
{
    public function addItem(array $data): Result;
    
}
