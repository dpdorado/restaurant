<?php

namespace App\Interfaces\Domain;

use App\Result\Result;

interface IPlate
{
    public function storeImagePlate($plate_id, array $images);
    public function deleteImagesPlate($plate_id);
}
