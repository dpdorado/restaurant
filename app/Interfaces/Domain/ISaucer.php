<?php

namespace App\Interfaces\Domain;

interface ISaucer
{
    public function storeImageSaucer($saucer_id, array $images);
    public function deleteImagesSaucer($saucer_id);
}
