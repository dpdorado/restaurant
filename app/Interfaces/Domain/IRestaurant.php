<?php

namespace App\Interfaces\Domain;

interface IRestaurant
{
    public function storeImageRestaurant($restaurant_id, array $images);
    public function deleteImagesRestaurant($restaurant_id);
}
