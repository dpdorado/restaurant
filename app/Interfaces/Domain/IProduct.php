<?php

namespace App\Interfaces\Domain;

use App\Result\Result;

interface IProduct
{
    public function storeImageProduct($product_id, array $images);
    public function deleteImagesProduct($product_id);
}
