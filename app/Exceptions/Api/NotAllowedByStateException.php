<?php

namespace App\Exceptions\Api;

use App\Exceptions\BaseException;
use Throwable;

class NotAllowedByStateException extends BaseException
{
    public function __construct(string $status = "FAILED", int $code = 480, Throwable $previous = null)
    {

        parent::__construct($status, $code, $previous);
        $this->result->setStatus($status);
        $this->result->setCode($code);
        $this->result->setDescription('Operation not allowed');
        $this->result->addMessage('ERR_STATE # The current type or state of the resource does not allow this operation');
    }
}
