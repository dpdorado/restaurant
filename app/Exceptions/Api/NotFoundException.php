<?php

namespace App\Exceptions\Api;

use App\Exceptions\BaseException;
use Exception;
use Throwable;

class NotFoundException extends BaseException
{
    public function __construct($status = "FAILED", $code = 404, Throwable $previous = null)
    {
        parent::__construct($status, $code, $previous);
        
        $this->result->setStatus($status);
        $this->result->setCode($code);
        $this->result->setDescription('not found');
        $this->result->addMessage('NOT_FOUND # The requested resource does not exist');
    }
}
