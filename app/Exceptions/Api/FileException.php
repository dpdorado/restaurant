<?php

namespace App\Exceptions\Api;

use App\Exceptions\BaseException;
use Throwable;

class FileException extends BaseException
{
    public function __construct(string $status = "FAILED", int $code = 490, Throwable $previous = null){

        parent::__construct($status, $code, $previous);
        $this->result->setStatus($status);
        $this->result->setCode($code);
        $this->result->setDescription('Problem loading/reading file');
        $this->result->addMessage('FILE_ERR # It´s not you, it´s us. We are having trouble processing the file.');
        
    }
}
