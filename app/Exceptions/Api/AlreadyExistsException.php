<?php

namespace App\Exceptions\Api;

use App\Exceptions\BaseException;
use Throwable;

class AlreadyExistsException extends BaseException
{
    public function __construct(string $status = "FAILED", int $code = 409, Throwable $previous = null)
    {

        parent::__construct($status, $code, $previous);
        $this->result->setStatus($status);
        $this->result->setCode($code);
        $this->result->setDescription('The resource or operation already exists');
        $this->result->addMessage('ALREADY_EXISTS # The resource or operation you are trying to store is already registered ');
    }
}
