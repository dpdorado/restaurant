<?php

namespace App\Exceptions\Api;

use App\Exceptions\BaseException;
use Throwable;

class UnauthorizedPassportException extends BaseException
{
    public function __construct(string $status = "FAILED", int $code = 401, Throwable $previous = null){

        parent::__construct($status, $code, $previous);
        $this->result->setStatus($status);
        $this->result->setCode($code);
        $this->result->setDescription('Unauthorized, please check your credentials');
        $this->result->addMessage('UNAUTHORIZED # the access credentials are not valid');
        
    }
}
