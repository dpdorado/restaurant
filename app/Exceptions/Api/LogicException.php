<?php

namespace App\Exceptions\Api;

use App\Exceptions\BaseException;
use Throwable;

class LogicException extends BaseException
{
    public function __construct($status = "FAILED", $code = 470, Throwable $previous = null)
    {
        parent::__construct($status, $code, $previous);
        
        $this->result->setStatus($status);
        $this->result->setCode($code);
        $this->result->setDescription('You do not have permissions for this operation');
        $this->result->addMessage('FORBIDDEN # prohibited for the requested resource');
    }
}
