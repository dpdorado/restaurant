<?php

namespace App\Exceptions\Api;

use App\Exceptions\BaseException;
use Throwable;

class CheckDataException extends BaseException
{
    public function __construct(string $status = "FAILED", int $code = 470, Throwable $previous = null)
    {

        parent::__construct($status, $code, $previous);
        $this->result->setStatus($status);
        $this->result->setCode($code);
        $this->result->setDescription('Your Request has erros');
        $this->result->addMessage('ERR_CHECK_DATA # The form has errors whit the inputs');
    }

    public function addErrors($erros)
    {
        $this->result->setAllError($erros);
    }
}
