<?php

namespace App\Exceptions;

use App\Result\Result;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException;
use Spatie\Permission\Exceptions\UnauthorizedException as SpatieUnauthorizedException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Throwable;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Register the exception handling callbacks for the application.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Throwable  $exception
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @throws \Throwable
     */

    public function render($request, Throwable $exception)
    {
        if ($exception instanceof BaseException) {
            return $exception->result->getJsonResponse();
        }
        if ($exception instanceof SpatieUnauthorizedException) {

            $result = new Result();
            $result->addMessage('FORBIDDEN # Forbidden for this role');
            $result->setDescription('Forbidden');
            $result->setStatus('FAILED');
            $result->setCode(403);

            return $result->getJsonResponse();
        } 

        if ($exception instanceof MethodNotAllowedHttpException) {
            $result = new Result();

            $result->setDescription('bad route');
            $result->addMessage('BAD_ROUTE # the type of method sent is not supported by this route');
            $result->setStatus('FAILED');
            $result->setCode(405);

            return $result->getJsonResponse();
        }
        if ($exception instanceof NotFoundHttpException) {

            $result = new Result();

            $result->setDescription('route not found');
            $result->addMessage('ROUTE_NOT_FOUND # The requested route does not exist');
            $result->setStatus('FAILED');
            $result->setCode(404);

            return $result->getJsonResponse();
        }

        if ($exception instanceof ModelNotFoundException) {

            $result = new Result();
            $result->setDescription('not found');
            $result->addMessage('NOT_FOUND # The requested resource does not exist');
            $result->setStatus('FAILED');
            $result->setCode(404);

            return $result->getJsonResponse();
        }

        return parent::render($request, $exception);
    }
}
